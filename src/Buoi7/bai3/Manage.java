package Buoi7.bai3;

import java.util.ArrayList;
import java.util.Scanner;

public class Manage {
    ArrayList<Bus> buses= new ArrayList<>();

    public Manage(ArrayList<Bus> buses) {
        this.buses = buses;
    }

    public Manage() {
    }
    public void Add_Bus()
    {
        Bus bus = new Bus();
        bus.Fill_Data();
        buses.add(bus);
    }
    public void Show_List_Bus()
    {
        for(int i=0; i<buses.size(); i++)
        {
            System.out.println((i+1)+ ". " +buses.toString());
        }
    }
    //3. Nhập vào tên 1 con đường và kiểm tra xem có bao nhiêu tuyến xe đi qua con đường đó. HIển thị danh sách các tuyến xe đó.
    public void Show_Bus_Across_Way()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("hập vào tên 1 con đường: ");
        String key_Way= sc.nextLine();
        for (Bus obj : buses) {
            if (obj.Across_Way(key_Way))
                System.out.println(obj.toString());
        }
    }

}
