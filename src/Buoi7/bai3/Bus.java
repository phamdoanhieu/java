package Buoi7.bai3;

import java.util.ArrayList;
import java.util.Scanner;

public class Bus {
    //Mã tuyến xe, số xe, danh sách các con đường sẽ đi qua.
    private String code;
    private String license_Plate;
    private ArrayList<String> way = new ArrayList<>();

    public Bus(String code, String license_Plate, ArrayList<String> way) {
        this.code = code;
        this.license_Plate = license_Plate;
        way = way;
    }

    public Bus() {

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLicense_Plate() {
        return license_Plate;
    }

    public void setLicense_Plate(String license_Plate) {
        this.license_Plate = license_Plate;
    }

    public ArrayList<String> getway() {
        return way;
    }

    public void setway(ArrayList<String> way) {
        way = way;
    }
    public void Fill_Data()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mã tuyến xe: ");
        code = sc.nextLine();
        System.out.println("Nhập số xe: ");
        license_Plate = sc.nextLine();
        System.out.println("Nhập danh sách các tuyến đường sẽ đi qua: ");
        while (true)
        {
            System.out.println("Nhập tên đường: ");
            String tmp = sc.nextLine();
            way.add(tmp);
            System.out.println("Bạn có muốn nhập tiếp không? Y/N");
            String key = sc.nextLine();
            if(key.toUpperCase().equals("N"))
            {
                break;
            }
        }
    }
    public boolean Across_Way(String key_Way)
    {
        for(int i=0; i<way.size(); i++)
        {
            if(way.get(i).equals(key_Way))
                return true;
        }
        return false;
    }
    @Override
    public String toString() {
        return "Bus{" +
                "code='" + code + '\'' +
                ", license_Plate='" + license_Plate + '\'' +
                ", way=" + way +
                '}';
    }
}
