package Buoi7.bai2;

import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);

    static void Menu() {
        System.out.println("1. Thêm 1 tác giả vào danh sách các tác giả, biết ràng 1 tác giả gồm các thuộc tính: Mã tác giả và tên tác giả.");
        System.out.println("2. Thêm 1 cuốn sách vào danh sách các cuốn sách biết rằng cuốn sách đó gồm các thuộc tính: Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản. Kiểm tra xem mã tác giả có tồn tại hay không? Nếu chưa có thì phải tạo tác giả rồi mới cho thêm vào danh sách.");
        System.out.println("3. Nhập vào tên 1 cuốn sách bất kì, kiểm tra xem cuốn sách đó có nằm trg danh sách hay không?");
        System.out.println("4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết");
        System.out.println("5. Thoát");
        System.out.println("Nhập lựa chọn của ban ");

    }

    public static void main(String[] args) {
//        Bài 2: Tạo và thực hiện các chức năng sau của Menu:
//        1. Thêm 1 tác giả vào danh sách các tác giả, biết ràng 1 tác giả gồm các thuộc tính: Mã tác giả và tên tác giả.
//        2. Thêm 1 cuốn sách vào danh sách các cuốn sách biết rằng cuốn sách đó gồm các thuộc tính: Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản. Kiểm tra xem mã tác giả có tồn tại hay không? Nếu chưa có thì phải tạo tác giả rồi mới cho thêm vào danh sách.
//        3. Nhập vào tên 1 cuốn sách bất kì, kiểm tra xem cuốn sách đó có nằm trg danh sách hay không?
//        4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết.
//        5. Thoát
        Manage manage = new Manage();
        int chon;
        while (true) {
            Menu();
            chon= sc.nextInt();
            switch (chon) {
                case 1:
                    System.out.println("1. Thêm 1 tác giả vào danh sách các tác giả");
                    manage.Add_Author();
                    break;
                case 2:
                    System.out.println("2. Thêm 1 cuốn sách vào danh sách các cuốn sách ");
                    manage.Add_Book();
                    break;
                case 3:
                    System.out.println("3. Nhập vào tên 1 cuốn sách bất kì, kiểm tra xem cuốn sách đó có nằm trg danh sách hay không?");
                    manage.Check_Book();
                    break;
                case 4:
                    System.out.println("4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết");
                    manage.Show_Book_of_Author();
                    break;
                case 5:
                    System.out.println("5. Thoát");
                    return;
            }
        }
    }
}