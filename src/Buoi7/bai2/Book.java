package Buoi7.bai2;

import java.util.Scanner;

public class Book {
    private String bookID;
    private String authorID;
    private String name;
    private String publication_day;
    private String publisher;

    public Book(String bookID, String authorID, String name, String publication_day, String publisher) {
        this.bookID = bookID;
        this.authorID = authorID;
        this.name = name;
        this.publication_day = publication_day;
        this.publisher = publisher;
    }

    public Book() {
    }

    public String getbookID() {
        return bookID;
    }

    public void setbookID(String bookID) {
        this.bookID = bookID;
    }

    public String getauthorID() {
        return authorID;
    }

    public void setauthorID(String authorID) {
        this.authorID = authorID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublication_day() {
        return publication_day;
    }

    public void setPublication_day(String publication_day) {
        this.publication_day = publication_day;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public void Fill_Data(){
        //Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản.
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mã sách : ");
        bookID = sc.nextLine();
        System.out.println("Nhập mã tác giả : ");
        authorID = sc.nextLine();
        System.out.println("Nhập tên sách : ");
        name = sc.nextLine();
        System.out.println("Nhập ngày/tháng/năm xuất bản : ");
        publication_day = sc.nextLine();
        System.out.println("Nhà sx: ");
        publisher = sc.nextLine();
    }
    @Override
    public String toString() {
        return "Book{" +
                "bookID='" + bookID + '\'' +
                ", authorID='" + authorID + '\'' +
                ", name='" + name + '\'' +
                ", publication_day='" + publication_day + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
