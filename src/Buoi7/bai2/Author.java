package Buoi7.bai2;

import java.util.Scanner;

public class Author {
    private String name;
    private String id;

    public Author(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public Author() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getid() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }
    public void Fill_Data(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập tên tác giả");
        name = sc.nextLine();
        System.out.println("Nhập mã : ");
        id = sc.nextLine();
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

}
