package Buoi7.bai2;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class Manage {
    ArrayList<Author> authorArrayList = new ArrayList<>();
    ArrayList<Book> bookArrayList = new ArrayList<>();

    public Manage(ArrayList<Author> authorArrayList, ArrayList<Book> bookArrayList) {
        this.authorArrayList = authorArrayList;
        this.bookArrayList = bookArrayList;
    }

    public Manage() {
    }

    //1. Thêm 1 tác giả vào danh sách các tác giả,
    public void Add_Author()
    {
        Author author = new Author();
        author.Fill_Data();
        authorArrayList.add(author);
    }

    public void Add_Book()
    {
        Book book = new Book();
        book.Fill_Data();
        AtomicBoolean Exist = new AtomicBoolean(false);
        authorArrayList.forEach((obj)->
        {
            if(obj.getid().equals(book.getauthorID()))
            {
                Exist.set(true);
            }
        });
        if (!Exist.get())// Nếu chưa có thì phải tạo tác giả rồi mới cho thêm vào danh sách.
            Add_Author();
        bookArrayList.add(book);
    }

    public void Check_Book()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập tên sách cần check: ");
        String key = sc.nextLine();
        AtomicBoolean Exist = new AtomicBoolean(false);
        bookArrayList.forEach(obj ->
        {
            if(obj.getName().equals(key))
                Exist.set(true);
        });
        if(Exist.get())
        {
            System.out.println("Có tồn tại");
        }
        else
        {
            System.out.println("Không tồn tại");
        }
    }
    public void Show_Book_of_Author()
    {
        //4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập tên tác giả: ");
        String key = sc.nextLine();
        String tmp_author_id = null;
        for (Author author : authorArrayList) {
            if (author.getid().equals(key))
                tmp_author_id = author.getid();
        }
        for(Book book : bookArrayList)
        {
            if(book.getauthorID().equals(tmp_author_id))
                System.out.println(book.toString());
        }
    }
}
