package Buoi7.bai1;

import java.util.Scanner;

//
//Bài 1: Tạo và thực hiện các chức năng sau của MENU:
//        1. Thêm 1 sinh viên vào danh sách biết rằng sinh viên gồm các thuộc tính: mã sinh viên, họ tên SV, tuổi, giới tính, quê quán.
//        2. Hiển thị danh sách sinh viên.
//        3. Hiển thị danh sách các sinh viên mang giới tính nữ
//        4. HIển thị danh sách sinh viên có tên là Tuấn
//        5. Nhập vào tên sinh viên và kiểm tra xem sinh viên đó có trg danh sách hay không? Nếu có thì hiển thị thông tin của sinh viên đó. Nếu không có thì tiến hành thêm mới sinh viên.
//        6. Thoát.
//
//        Bài
public class Student {
    protected String maSV;
    protected String hoTen;
    protected int tuoi;
    protected boolean gioiTinh;
    protected String queQuan;

    public Student(String maSV, String hoTen, int tuoi, boolean gioiTinh, String queQuan) {
        this.maSV = maSV;
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.gioiTinh = gioiTinh;
        this.queQuan = queQuan;
    }

    public Student() {

    }
    public void Fill_Data()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mssv ");
        maSV = sc.nextLine();
        System.out.println("Nhập tên : ");
        hoTen = sc.nextLine();
        System.out.println("Nhập tuổi : ");
        tuoi = sc.nextInt();
        System.out.println("Nhập giới tính : true = nam, false = nữ ");
        sc.nextLine();
        gioiTinh = sc.nextBoolean();
        sc.nextLine(); // clear enter
        System.out.println("Nhập địa chỉ : ");
        queQuan = sc.nextLine();
    }
    public String getMaSV() {
        return maSV;
    }

    public void setMaSV(String maSV) {
        this.maSV = maSV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public boolean isGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(boolean gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    @Override
    public String toString() {
        return  "[Mã sinh viên='" + maSV + '\'' +
                ", Họ và tên='" + hoTen + '\'' +
                ", Tuổi=" + tuoi +
                ", Giới tính=" + gioiTinh +
                ", Quê quán='" + queQuan + '\'' +
                ']';
    }
}
