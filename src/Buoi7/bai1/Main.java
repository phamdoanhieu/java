package Buoi7.bai1;

import java.awt.*;
import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);

    static void Menu() {
        System.out.println("1. Thêm 1 sinh viên vào danh sách biết rằng sinh viên");
        System.out.println("2. Hiển thị danh sách sinh viên.");
        System.out.println("3. Hiển thị danh sách các sinh viên mang giới tính nữ");
        System.out.println("4. HIển thị danh sách sinh viên có tên là Tuấn");
        System.out.println("5. Nhập vào tên sinh viên và kiểm tra xem sinh viên đó có trg danh sách hay không? Nếu có thì hiển thị thông tin của sinh viên đó. Nếu không có thì tiến hành thêm mới sinh viên.");
        System.out.println("6. Thoát.");

        System.out.println("Nhập lựa chọn của bạn");
    }

    public static void main(String[] args) {
        Manage manage = new Manage();
        while (true) {
            Menu();
            int chon = sc.nextInt();
            switch (chon) {
                case 1:
                    manage.Add_Student();
                    break;
                case 2:
                    System.out.println("2. Hiển thị danh sách sinh viên");
                    manage.Show_List_Student();
                    break;
                case 3:
                    System.out.println("3. Hiển thị danh sách các sinh viên mang giới tính nữ");
                    manage.Show_Female();
                    break;
                case 4:
                    System.out.println("4. HIển thị danh sách sinh viên có tên là Tuấn");
                    manage.Show_Tuan();
                    break;
                case 5:
                    System.out.println("5. Nhập vào tên sinh viên và kiểm tra xem sinh viên đó có trg danh sách hay không? Nếu có thì hiển thị thông tin của sinh viên đó. Nếu không có thì tiến hành thêm mới sinh viên.");
                    manage.Check_Name();
                    break;
                case 6:
                    System.out.println("6. Thoát.");
                    return;
            }
        }
    }
}
