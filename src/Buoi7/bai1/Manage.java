package Buoi7.bai1;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class Manage {
    Scanner sc = new Scanner(System.in);
    public ArrayList<Student> studentArrayList = new ArrayList<>();

    public Manage(ArrayList<Student> studentArrayList) {
        this.studentArrayList = studentArrayList;
    }

    public Manage() {

    }

    public void Add_Student()
    {
        Student tmp = new Student();
        tmp.Fill_Data();
        studentArrayList.add(tmp);
    }
    public void Show_List_Student()
    {
        //        2. Hiển thị danh sách sinh viên.
        for(int i=0; i<studentArrayList.size(); i++)
        {
            System.out.println(i+ ". " + studentArrayList.get(i).toString());
        }
    }
    ////        3. Hiển thị danh sách các sinh viên mang giới tính nữ
    public void Show_Female()
    {

        studentArrayList.forEach((obj) ->{
            if(!obj.isGioiTinh())
            {
                System.out.println(obj.toString());
            }
        });
    }
    ////        4. HIển thị danh sách sinh viên có tên là Tuấn
    public void Show_Tuan()
    {
        studentArrayList.forEach((obj)->
        {
            if(obj.hoTen.contains("Tuấn"))
            {
                System.out.println(obj.toString());
            }
        });
    }
    public void Check_Name()
    {
        System.out.println("Nhập sinh viên cần kiêm tra");
        String key = sc.nextLine();
        AtomicBoolean exist= new AtomicBoolean(false);
        studentArrayList.forEach((obj)->
        {
            if(obj.hoTen.equals(key))
            {
                System.out.println("Có tồn tại sinh viên " + key);
                System.out.println(obj.toString());
                exist.set(true);
            }
        }
        );
        if(!exist.get())
        {
            Add_Student();
        }
    }
}
