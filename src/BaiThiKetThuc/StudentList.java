package BaiThiKetThuc;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.*;

public class StudentList {

// check file co rong ko

    ArrayList<Student> studentArrayList = new ArrayList<>();

    public StudentList() {
    }

    public static JSONObject GetAllofJSONobj(JSONArray array) {
        JSONObject DataJSON = new JSONObject();
        for (int i = 0; i < array.size(); i++) {
            Map infor = (Map) array.get(i);
            Iterator<Map.Entry> itr1 = infor.entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                DataJSON.put(pair.getKey(), pair.getValue());
            }
        }
        return DataJSON;
    }


    public void Add_Student() {
        Student tmp = new Student();
        tmp.Fill_Info();
        studentArrayList.add(tmp);

    }

    public void Show_Student_List() {
        System.out.println("\tDanh sách sinh viên");
        for (int i = 0; i < studentArrayList.size(); i++) {
            System.out.println("----So TT: " + (i + 1) + "\n" + studentArrayList.get(i).toString());
        }
    }

    // nhaapj vao 1 khoa, sap xep va hien thi khoa theo ds sinh vien ?
    public void HienThiTatCaSVTrong1Khoa() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập khoa có học sinh cần hiển thị: ");
        String khoa_Key = sc.nextLine();
        ArrayList<Student> DS_SV_theo_Khoa = new ArrayList<>();

        // lấy ds học sinh theo khoa
        for (int i = 0; i < studentArrayList.size(); i++) {
            if (studentArrayList.get(i).getDepartment().equals(khoa_Key)) {

                DS_SV_theo_Khoa.add(studentArrayList.get(i));
            }
        }

        // sắp xếp theo tên
        for (int i = 0; i < DS_SV_theo_Khoa.size() - 1; i++) {
            for (int j = i + 1; j < DS_SV_theo_Khoa.size(); j++) {
                if (DS_SV_theo_Khoa.get(i).getFullName().compareTo(DS_SV_theo_Khoa.get(j).getFullName()) > 0) {
                    Collections.swap(DS_SV_theo_Khoa, i, j);
                }
            }
        }

        // hiển thị;
        System.out.println("\tDanh sách sinh viên của khoa " + khoa_Key);
        for (int i = 0; i < DS_SV_theo_Khoa.size(); i++) {
            System.out.println("----So TT: " + (i + 1) + "\n" + DS_SV_theo_Khoa.get(i).toString());
        }
    }

    public void FindStudentID() {
        int i;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mã Sinh Viên cần tìm: ");
        String studentID_Key = sc.nextLine();
        Boolean found = false;
        for (i = 0; i < studentArrayList.size(); i++) {
            if (studentArrayList.get(i).getStudentID().equals(studentID_Key)) {
                found = true;
                break;
            }
        }

        if (found)
            System.out.println(studentArrayList.get(i).toString());
        else
            System.out.println("Không tìm thấy Sinh Viên có mã: " + studentID_Key);
    }

    public void FindStudent_With_Name() {

        Scanner sc = new Scanner(System.in);
        int i;
        System.out.println("Nhập tên Sinh Viên cần tìm: ");
        String studentName_Key = sc.nextLine();
        Boolean found = false;
        for (i = 0; i < studentArrayList.size(); i++) {
            if (studentArrayList.get(i).getFullName().equals(studentName_Key)) {
                found = true;
                break;
            }
        }

        if (found) {
            studentArrayList.remove(i);
            System.out.println("Đã xoá sinh viên có tên " + studentName_Key + " ra khỏi danh sách");
            Show_Student_List();
        } else
            System.out.println("Không tìm thấy Sinh Viên có tên: " + studentName_Key);
    }
}
