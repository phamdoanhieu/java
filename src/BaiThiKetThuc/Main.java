package BaiThiKetThuc;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);

    static void Menu() {
        System.out.println("1. Thêm 1 sinh viên vào danh sách");
        System.out.println("2. Hiển thị danh sách sinh viên.");
        System.out.println("3. Hiển thị danh sách các sinh viên theo khoa nhập vào");
        System.out.println("4. Hiển thị sinh viên từ mã SV");
        System.out.println("5. Kiểm tra SV có tồn tại ko ? Có -> xoá khỏi DS");
        System.out.println("6. Thoát.");

        System.out.println("Nhập lựa chọn của bạn");
    }

    public static void main(String[] args) throws IOException, ParseException {
        JSONManage jsonManage = new JSONManage();

        while (true) {
            Menu();
            int chon = sc.nextInt();
            switch (chon) {
                case 1:
                    jsonManage.getStudentList().Add_Student();
                    jsonManage.JSONtoFile();
                    break;
                case 2:
                    System.out.println("2. Hiển thị danh sách sinh viên");
                    jsonManage.getStudentList().Show_Student_List();
                    break;
                case 3:
                    System.out.println("3. Hiển thị danh sách các sinh viên theo khoa nhập vào");
                    jsonManage.getStudentList().HienThiTatCaSVTrong1Khoa();
                    break;
                case 4:
                    System.out.println("4. Hiển thị sinh viên từ mã SV");
                    jsonManage.getStudentList().FindStudentID();
                    break;
                case 5:
                    System.out.println("5. Kiểm tra SV có tồn tại ko ? Có -> xoá khỏi DS");
                    jsonManage.getStudentList().FindStudent_With_Name();
                    jsonManage.JSONtoFile();
                    break;
                case 6:
                    System.out.println("6. Thoát.");
                    return;
            }
        }
    }
}
