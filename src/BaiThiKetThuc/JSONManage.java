package BaiThiKetThuc;

import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Formatter;

public class JSONManage {
    private StudentList studentList = new StudentList();

    public StudentList getStudentList() {
        return studentList;
    }

    public void setStudentList(StudentList studentList) {
        this.studentList = studentList;
    }

    public void JSONtoFile() {
        Gson gson = new Gson();
        try {
            String currentLocation = System.getProperty("user.dir") + "/src/";
            String dir = currentLocation + "BaiThiKetThuc/test.json";
            Formatter formatter = new Formatter(dir);
            formatter.format("%s", gson.toJson(this.studentList));
            formatter.close();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
    }

    public JSONObject ReadFileJSON() throws IOException, ParseException {
        String currentLocation = System.getProperty("user.dir") + "/src/";
        String dir = currentLocation + "BaiThiKetThuc/test.json";
        JSONObject obj = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(dir)));
        return obj;
    }

    public JSONManage() {
        Gson gson = new Gson();
        // nếu tồn tại dữ liệu có sẵn
        try {
            // add data
            studentList = gson.fromJson(String.valueOf(ReadFileJSON()), StudentList.class);
        } catch (IOException | ParseException ignored) // file ko tồn tại
        {

        }
    }
}
