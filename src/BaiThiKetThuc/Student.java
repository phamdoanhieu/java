package BaiThiKetThuc;

import java.util.Scanner;

public class Student {
    private String fullName;
    private String studentID;
    private String dateOfBirth;
    private String address;
    private String studenClass;
    private String khoa;
    private String department;


    public Student(String fullName, String studentID, String dateOfBirth, String address, String studenClass, String khoa, String department, Scanner sc) {
        this.fullName = fullName;
        this.studentID = studentID;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.studenClass = studenClass;
        this.khoa = khoa;
        this.department = department;
    }

    public Student() {
    }

    public boolean verifyName(String obj) {
        String name_PATTERN = "^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
                "ẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
                "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]{1,50}$";
        if (obj == null) return false;
        return obj.matches(name_PATTERN);
    }

    public boolean verifyID(String obj) {
        String PATTERN = "^[a-zA-Z_0-9]+$";
        if (obj == null) return false;
        return obj.matches(PATTERN);
    }

    public boolean verifyDate(String obj) {
        String PATTERN = "\\b(0[1-9]|1\\d|2\\d|3[01])\\/(0[1-9]|1[0-2])\\/(\\d{4})\\b";
        /*
        \b thay the ^ va $
        ngày: 01->09 || 10_>19 || 20->29 || (30,31)
        phân cách \/ = \
        tháng ràng buộc 01-09 || (10,11,12)
        năm:  \d{4} = 0000 ->9999
         */
        if (obj == null) return false;
        return obj.matches(PATTERN);
    }
    public boolean verifyDepartment(String obj) {
        String PATTERN = "\\b(AT)|(CT)|(DT)\\b";
        if (obj == null) return false;
        return obj.matches(PATTERN);
    }

    public void Fill_Info() {
        Boolean flag = false;
        Scanner sc = new Scanner(System.in);

        while (!flag) {
            System.out.println("Nhập mã sinh viên: ");
            studentID = sc.nextLine();
            flag = verifyID(studentID);
        }
        flag = false;
        while (!flag) {
            System.out.println("Nhập họ và tên: ");
            fullName = sc.nextLine();
            flag = verifyName(fullName);
        }
        flag = false;
        while (!flag) {
            System.out.println("Nhập ngày sinh (dd/mm/yyyy): ");
            dateOfBirth = sc.nextLine();
            flag = verifyDate(dateOfBirth);
        }
        flag = false;
        while (!flag) {

            System.out.println("Nhập quê quán: ");
            address = sc.nextLine();
            flag = verifyName(address);
        }
        flag = false;
        while (!flag) {
            System.out.println("Nhập lớp: ");
            studenClass = sc.nextLine();
            flag = verifyID(studenClass);
        }
        flag = false;
        while (!flag) {
            System.out.println("Nhập niên khoá: ");
            khoa = sc.nextLine();
            flag = verifyID(khoa);
        }
        flag = false;
        while (!flag) {

            System.out.println("Nhập khoa (AT|CT|DT): ");
            department = sc.nextLine();
            flag = verifyDepartment(department);
        }


    }

//    @Override
//    public String toString() {
//        return "{" +
//                "\"fullName\":\"" + fullName + '\"' +
//                ",\"studentID\":\"" + studentID + '\"' +
//                ",\"dateOfBirth\":\"" + dateOfBirth + '\"' +
//                ",\"address\":\"" + address + '\"' +
//                ",\"studenClass\":\"" + studenClass + '\"' +
//                ",\"khoa\":\"" + khoa + '\"' +
//                ",\"department\":\"" + department + '\"' +
//                '}';
//    }


    @Override
    public String toString() {
        return "{" +
                "fullName='" + fullName + '\'' +
                ", studentID='" + studentID + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", address='" + address + '\'' +
                ", studenClass='" + studenClass + '\'' +
                ", khoa='" + khoa + '\'' +
                ", department='" + department + '\'' +
                '}';
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStudenClass() {
        return studenClass;
    }

    public void setStudenClass(String studenClass) {
        this.studenClass = studenClass;
    }

    public String getKhoa() {
        return khoa;
    }

    public void setKhoa(String khoa) {
        this.khoa = khoa;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
