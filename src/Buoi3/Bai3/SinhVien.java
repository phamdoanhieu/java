package Buoi3.Bai3;
//- Mã sinh viên
//        - Họ tên
//        - Quê quán
//        - Điểm thi môn toán A1
//        - Điểm thi môn toán A2
//        - Điểm thi môn toán A3
//        - Điểm thi môn tin học Đại cương
//        - Điểm thi môn kĩ thuật lập trình


import java.util.Scanner;

public class SinhVien {
    private String SinhVien_HoTen;
    private String SinhVien_QueQuan;
    private float Diem_ToanA1;
    private float Diem_ToanA2;
    private float Diem_ToanA3;
    private float Diem_TinHocDaiCuong;
    private float Diem_KiThuatLapTrinh;
    private String Mon_CaoNhat;
    private float Diem_CaoNhat;
    private String Mon_ThapNhat;
    private float Diem_ThapNhat;

    public void Nhap_Data() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập tên sinh viên: ");
        this.SinhVien_HoTen = sc.nextLine();
        System.out.println("Nhập quê quán: ");
        this.SinhVien_QueQuan = sc.nextLine();
        System.out.println("Nhập điểm toán A1");
        this.Diem_ToanA1 = sc.nextFloat();
        System.out.println("Nhập điểm toán A2");
        this.Diem_ToanA2 = sc.nextFloat();
        System.out.println("Nhập điểm toán A3");
        this.Diem_ToanA3 = sc.nextFloat();
        System.out.println("Nhập điểm Tin học đại cương: ");
        this.Diem_TinHocDaiCuong = sc.nextFloat();
        System.out.println("Nhập điểm Kĩ thuật lập trình: ");
        this.Diem_KiThuatLapTrinh = sc.nextFloat();
    }

    //        Tạo menu với các chức năng sau:
//        1. Viết hoa tên của đối tượng sinh viên vừa nhập vào
//        2. In ra màn hình điểm cao nhất của đối tượng
//        3. In ra màn hình điểm thấp nhất của đối tượng
//        4. Sửa lại điểm thi 1 môn bất kì và kiểm tra lại môn nào có điểm thi cao nhất
//        5. Thoát

    public void VietHoaTen() {
        this.SinhVien_HoTen = this.SinhVien_HoTen.toUpperCase();
        System.out.println("Tên sau khi viết hoa: " + this.SinhVien_HoTen);
    }

    public void Diem_CaoNhat() {
        this.Diem_CaoNhat = this.Diem_ToanA1;
        this.Mon_CaoNhat = "Toán A1";
        if (this.Diem_ToanA2 >= this.Diem_CaoNhat) {
            if (this.Diem_ToanA2 != this.Diem_CaoNhat)
                this.Mon_CaoNhat = "Toán A2";
            else this.Mon_CaoNhat += " Và Toán A2";

            this.Diem_CaoNhat = this.Diem_ToanA2;

        }
        if (this.Diem_ToanA3 >= this.Diem_CaoNhat) {
            if (this.Diem_ToanA3 != this.Diem_CaoNhat)
                this.Mon_CaoNhat = "Toán A3";
            else this.Mon_CaoNhat = " Và Toán A3";

            this.Diem_CaoNhat = this.Diem_ToanA3;

        }

        if (this.Diem_KiThuatLapTrinh >= this.Diem_CaoNhat) {

            if (this.Diem_KiThuatLapTrinh != this.Diem_CaoNhat)
                this.Mon_CaoNhat = "KiThuatLapTrinh";
            else this.Mon_CaoNhat += " Và KiThuatLapTrinh";

            this.Diem_CaoNhat = this.Diem_KiThuatLapTrinh;

        }
        if (this.Diem_TinHocDaiCuong >= this.Diem_CaoNhat) {
            if (this.Diem_TinHocDaiCuong != this.Diem_CaoNhat)
                this.Mon_CaoNhat = "TinHocDaiCuong";
            else this.Mon_CaoNhat += " Và TinHocDaiCuong";

            this.Diem_CaoNhat = this.Diem_TinHocDaiCuong;


        }

        System.out.println("Điểm cao nhất là: " + this.Diem_CaoNhat);
    }


    public void Diem_ThapNhat() {
        this.Diem_ThapNhat = this.Diem_ToanA1;
        this.Mon_CaoNhat = "Toán A1";
        if (this.Diem_ToanA2 <= this.Diem_ThapNhat) {
            if (this.Diem_ToanA2 != this.Diem_ThapNhat)
                this.Mon_CaoNhat = "Toán A2";
            else this.Mon_CaoNhat += " Và Toán A2";

            this.Diem_ThapNhat = this.Diem_ToanA2;

        }
        if (this.Diem_ToanA3 <= this.Diem_ThapNhat) {
            if (this.Diem_ToanA3 != this.Diem_ThapNhat)
                this.Mon_CaoNhat = "Toán A3";
            else this.Mon_CaoNhat += " Và Toán A3";
            this.Diem_ThapNhat = this.Diem_ToanA3;

        }
        if (this.Diem_KiThuatLapTrinh <= this.Diem_ThapNhat) {
            if (this.Diem_KiThuatLapTrinh != this.Diem_ThapNhat)
                this.Mon_CaoNhat = "KiThuatLapTrinh";
            else this.Mon_CaoNhat += " Và KiThuatLapTrinh";

            this.Diem_ThapNhat = this.Diem_KiThuatLapTrinh;


        }
        if (this.Diem_TinHocDaiCuong <= this.Diem_ThapNhat) {
            if (this.Diem_TinHocDaiCuong != this.Diem_ThapNhat)
                this.Mon_CaoNhat = "TinHocDaiCuong";
            else this.Mon_CaoNhat += " Và TinHocDaiCuong";

            this.Diem_ThapNhat = this.Diem_TinHocDaiCuong;


        }
        System.out.println("Điểm thấp nhất là: " + this.Diem_ThapNhat);
    }

    public void SuaDiem() {
        Scanner sc = new Scanner(System.in);
        System.out.println("1. Sửa điểm toán A1 ");
        System.out.println("2. Sửa điểm toán A2 ");
        System.out.println("3. Sửa điểm toán A3 ");
        System.out.println("4. Sửa điểm Kĩ thuật lập trình ");
        System.out.println("5. Sửa điểm Tin học đại cương ");
        System.out.println("Nhập môn cần sửa điểm: ");
        int chon = sc.nextInt();
        System.out.println("Nhập điểm mới: ");
        switch (chon) {
            case 1:
                this.Diem_ToanA1 = sc.nextFloat();
                break;
            case 2:
                this.Diem_ToanA2 = sc.nextFloat();
                break;
            case 3:
                this.Diem_ToanA3 = sc.nextFloat();
                break;
            case 4:
                this.Diem_KiThuatLapTrinh = sc.nextFloat();
                break;
            case 5:
                this.Diem_TinHocDaiCuong = sc.nextFloat();
        }

        System.out.print("Sau khi sửa ");
        Diem_CaoNhat();
        System.out.println("Môn cao nhất là: " +Mon_CaoNhat);
    }

}
