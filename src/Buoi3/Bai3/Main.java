package Buoi3.Bai3;


import java.util.Scanner;

public class Main {
    public static void Menu()
    {
        System.out.println(" ____________________________");
        System.out.println("|            Menu            |");
        System.out.println("|----------------------------|");
        System.out.println("| 1. Viết hoa tên Sinh Vien  |");
        System.out.println("| 2. In điểm cao nhất        |");
        System.out.println("| 3. In điểm thấp nhất       |");
        System.out.println("| 4. Sửa điểm                |");
        System.out.println("| 5. Thoát                   |");
        System.out.println("|----------------------------|");
        System.out.println("| Mời bạn chọn:              |");
        System.out.println(" ---------------------------- ");
    }
    public static void main(String[] args) {
        SinhVien sinhVien = new SinhVien();
        sinhVien.Nhap_Data();

        Scanner sc = new Scanner(System.in);
        while (true) {
            Menu();
            int chon = sc.nextInt();
            switch (chon) {
                case 1:
                    sinhVien.VietHoaTen();
                    break;
                case 2:
                    sinhVien.Diem_CaoNhat();
                    break;
                case 3:
                    sinhVien.Diem_ThapNhat();
                    break;
                case 4:
                    sinhVien.SuaDiem();
                    break;
                case 5:
                    return;
            }
        }
    }
}
