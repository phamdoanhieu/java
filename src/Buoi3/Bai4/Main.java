package Buoi3.Bai4;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void Show_list(ArrayList<Member> list) {
        System.out.println("Danh sách thành viên");
        System.out.printf("%5s|%-25s|%10s|%10s|%10s|%10s|\n", "ID", "Họ và tên", "Giói tính", "Năm sinh", "Ban/Team", "Chức vụ");
        for (int i = 0; i < list.size(); i++) {
            list.get(i).Show_Mem();
        }
    }

    public static void ThemThanhVien(ArrayList<Member> list) {
        Member member = new Member();
        member.Add_member();

        member.setMember_id(Integer.toString(list.size()+1));
        list.add(member);
    }

    public static void LayChong(ArrayList<Member> list) {
        System.out.println("List thành viên nữ lấy chồng được :D ");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getMember_sex().equals("nu") || list.get(i).getMember_sex().equals("nữ") || list.get(i).getMember_sex().equals("Nữ")|| list.get(i).getMember_sex().equals("Nu")) {
                if (list.get(i).LayChongDuocKhong()) {
                    System.out.println(list.get(i).getMember_name());
                }
            }
        }
    }
    //4. Hiển thị danh sách các thành viên của Web/ Arduino.
    public static void Show_Web_Ar(ArrayList<Member> list)
    {
        System.out.println("Danh sách thành viên web");
        int Dem=1;
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getMember_team().equals("Web")){
                System.out.println((Dem++)+ ". "+list.get(i).getMember_name());
            }
        }
        Dem=1;
        System.out.println("\nDanh sách thành viên Arduino");
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getMember_team().equals("Arduino")){
                System.out.println((Dem++)+". "+list.get(i).getMember_name());
            }
        }
    }
    //5. Thống kê số thành viên KIT sinh năm 2000
    public static void Age_2000(ArrayList<Member> list)
    {
        int Dem=0;
        System.out.println("Danh sách thành viên sinh năm 2000");
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getMember_birthday() == 2000){
                System.out.println((Dem++)+". "+list.get(i).getMember_name());
            }
        }
    }


    public static void Menu() {
        System.out.println(" _______________________________________________________");
        System.out.println("|                           Menu                        |");
        System.out.println("|-------------------------------------------------------|");
        System.out.println("| 1. Thêm thành viên KIT                                |");
        System.out.println("| 2. Hiển thị danh sách thành viên                      |");
        System.out.println("| 3. Hiển thị danh sách các bạn nữ đủ tuổi lấy chồng    |");
        System.out.println("| 4. Hiển thị danh sách các thành viên của Web/ Arduino |");
        System.out.println("| 5. Thống kê số thành viên KIT sinh năm 2000           |");
        System.out.println("| 6. Thoát                                              |");
        System.out.println("|-------------------------------------------------------|");
        System.out.println("| Mời bạn chọn:                                         |");
        System.out.println(" ------------------------------------------------------- ");
    }
    public static void main(String[] args) {
        ArrayList<Member> list = new ArrayList<Member>();
        Scanner sc = new Scanner(System.in);
        while (true) {
            Menu();
            int chon = sc.nextInt();
            switch (chon) {
                case 1:
                    ThemThanhVien(list);
                    break;
                case 2:
                    Show_list(list);
                    break;
                case 3:
                    LayChong(list);
                    break;
                case 4:
                    Show_Web_Ar(list);
                    break;
                case 5:
                    Age_2000(list);
                    break;
                case 6:
                    System.out.println("Đã thoát chương trình !");
                    return;
            }
        }
    }
}
