package Buoi3.Bai4;

import java.util.Calendar;
import java.util.Scanner;

//Bài 4: Sử dụng kiến thúc về mảng, tạo danh sách thành viên KIT. Đối tượng thành viên KIT gồm các thuộc tính sau:
//        - Mã thành viên
//        - Họ và tên
//        - Giới tính
//        - Năm sinh
//        - Ban/Team
//        - Chức vụ.
//        Tạo menu với các chức năng sau:
//        1. Lập danh sách thành viên KIT
//        2. Hiển thị danh sách thành viên
//        3. Hiển thị danh sách các bạn nữ đủ tuổi lấy chồng
//        4. Hiển thị danh sách các thành viên của Web/ Arduino.
//        5. Thống kê số thành viên KIT sinh năm 2000
//        6. Thoát
public class Member {
    private String Member_id;
    private String Member_name;
    private String Member_sex;
    private int Member_birthday;
    private String Member_team;
    private String Member_position;

    public String getMember_name() {
        return Member_name;
    }

    public String getMember_sex() {
        return Member_sex;
    }

    public String getMember_team() {
        return Member_team;
    }

    public int getMember_birthday() {
        return Member_birthday;
    }

    public void Add_member() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập thông tin thành viên");
        System.out.println("Nhập họ tên: ");
        this.Member_name = sc.nextLine();
        System.out.println("Nhập giới tính (Nam/Nữ): ");
        this.Member_sex = sc.nextLine();
        System.out.println("Nhập năm sinh: ");
        this.Member_birthday = sc.nextInt();
        sc.nextLine();// clear
        System.out.println("Thuộc ban/team nào ? (Arduino/Web/Mobile): ");
        this.Member_team = sc.nextLine();

        System.out.println("Chức vụ trong club: ");
        this.Member_position = sc.nextLine();
    }

    public void Show_Mem() {
        System.out.printf("%5s|%-25s|%10s|%10s|%10s|%10s|\n", this.Member_id, this.Member_name, this.Member_sex, this.Member_birthday, this.Member_team, this.Member_position);

    }

    public int Get_Age() {
        int age = 0;
        Calendar now = Calendar.getInstance();
        age = now.get(Calendar.YEAR) - this.Member_birthday;
        return age;
    }

    public void setMember_id(String member_id) {
        Member_id = member_id;
    }

    public boolean LayChongDuocKhong() {
        if (Get_Age() > 18)
            return true;
        return false;

    }
}

