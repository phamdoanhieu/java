package Buoi3.Bai2;

import java.util.Scanner;

//Bài 2: Tạo 1 đối tượng sản phẩm với các thuộc tính:
//        - Mã sản phẩm
//        - Tên sản phẩm
//        - Giá mua vào
//        - Giá bán ra
//        - Số lượng sản phẩm
//        Hỏi nếu bán hết toàn bộ sản phẩm đó trong
//        kho thì chủ cửa hàng sẽ lãi bao nhiêu tiền. Nếu chỉ bán hết 2/3 số lượng hàng thì chủ hàng sẽ lỗ/lãi bao nhiêu tiền.
public class SanPham {
    private String SanPham_ID;
    private String SanPhan_Ten;
    private int SanPham_GiaMua;
    private int SanPham_GiaBan;
    private int SanPham_SoLuong;

    public void NhapSP(){
        Scanner sc= new Scanner(System.in);
        System.out.println("Nhập mã sản phẩm: ");
        this.SanPham_ID = sc.nextLine();
        System.out.println("Nhập tên sản phẩm: ");
        this.SanPhan_Ten = sc.nextLine();
        System.out.println("Nhập giá mua vào: ");
        this.SanPham_GiaMua= sc.nextInt();
        System.out.println("Nhập giá bán ra: ");
        this.SanPham_GiaBan= sc.nextInt();
        System.out.println("Nhập số lượng sản phẩm: ");
        this.SanPham_SoLuong = sc.nextInt();
    }
    //Hỏi nếu bán hết toàn bộ sản phẩm đó trong kho thì chủ cửa hàng sẽ lãi bao nhiêu tiền.
    public int Lai()
    {
        int lai=0;
        lai = (this.SanPham_GiaBan-this.SanPham_GiaMua)*this.SanPham_SoLuong;
        return lai;
    }

    public void BanHet()
    {
        System.out.println("Bán hết tất cả sản phẩm sẽ lãi "+ Lai() +" đồng");
    }

    public void Ban2_3SP()
    {
        // Nếu chỉ bán hết 2/3 số lượng hàng thì chủ hàng sẽ lỗ/lãi bao nhiêu tiền.
        System.out.println("Ban 2/3 thì sẽ lãi " + (float) 2/3*Lai() + " đồng");
    }


}
