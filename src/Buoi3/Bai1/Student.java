package Buoi3.Bai1;

//- mã sinh viên
//        - tên sinh viên
//        - quê quán
//        - điểm thi môn toán cao cấp A1
//        - điểm thi môn toán cao cấp A3
//        - điểm môn nguyên lí 1
// đtb ,, tien thi lai

import java.util.Scanner;

public class Student {
    private int student_id;
    private String student_name;
    private String student_que;
    private float Point_Math_a1;
    private float Point_Math_a3;
    private float Point_NL1;


    // nhập xuất dữ
    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_que() {
        return student_que;
    }

    public void setStudent_que(String student_que) {
        this.student_que = student_que;
    }

    public float getPoint_Math_a1() {
        return Point_Math_a1;
    }

    public void setPoint_Math_a1(float point_Math_a1) {
        Point_Math_a1 = point_Math_a1;
    }

    public float getPoint_Math_a3() {
        return Point_Math_a3;
    }

    public void setPoint_Math_a3(float point_Math_a3) {
        Point_Math_a3 = point_Math_a3;
    }

    public float getPoint_NL1() {
        return Point_NL1;
    }

    public void setPoint_NL1(float point_NL1) {
        Point_NL1 = point_NL1;
    }

    //phương thức:

    public float DTB()
    {
        float dtb= (this.Point_Math_a1 + this.Point_Math_a3  + this.Point_NL1 )/3;
        return dtb;
    }
    public int TienHocLai()
    {
        int tien=0;
        if(this.Point_NL1 <4)
        {
            tien+=90;
        }
        if(this.Point_Math_a1 <4)
        {
            tien+=90;
        }
        if(this.Point_Math_a3 <4)
        {
            tien+=90;
        }
        return tien;
    }

    public void Nhap_Info()
    {
        Scanner sc = new Scanner(System.in);

        System.out.println("Nhap ma sinh vien: ");
        this.student_id = sc.nextInt();
        sc.nextLine(); // clear /n

        System.out.println("Nhap ho ten: ");
        this.student_name= sc.nextLine();
        System.out.println("Nhap que quan: ");
        this.student_que= sc.nextLine();

        System.out.println("Nhap diem toan a1: ");
        this.Point_Math_a1 = sc.nextFloat();
        System.out.println("Nhap diem toan a3: ");
        this.Point_Math_a3 = sc.nextFloat();
        System.out.println("Nhap diem Nguyen ly 1: ");
        this.Point_NL1 = sc.nextFloat();

    }
    public void Show_Info()
    {
        System.out.println("Ma hoc sinh: " + this.getStudent_id());
        System.out.println("Ho va ten: " + this.getStudent_name());
    }
    public void Show_Dtb()
    {
        System.out.println("Điểm trung bình : " + DTB());
    }

    public void Show_Tien_Thi_Lai()
    {
        System.out.println("Tiền thi lại là: " + TienHocLai() + "K");
    }


}
