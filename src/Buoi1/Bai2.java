package Buoi1;
import java.util.Scanner;

//Bài 2: Đếm số lượng số lẻ của số nguyên dương n
public class Bai2 {
    public static void main(String[] args) {
        int number,temp;
        int dem = 0;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();

        System.out.print("So luong so le cua so nguyen duong " + number);
        while (number>0)
        {
            temp= number%10;
            number=number/10;
            if (temp%2==0)
            {
                dem++;
            }
        }
        System.out.println(" la: "+dem);
    }
}
