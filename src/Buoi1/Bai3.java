package Buoi1;
import java.util.Scanner;
//Bài 3: Tìm chữ số lớn nhất của số nguyên dương n.
public class Bai3 {
    public static void main(String[] args) {
        int number,chuso;
        int max = 0;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();

        System.out.print("Chu so lon nhat cua so nguyen duong " + number);
        while (number>0)
        {
            chuso= number%10;
            number=number/10;
            max= (max<chuso) ? chuso : max;
        }
        System.out.println(" la: "+max);
    }
}
