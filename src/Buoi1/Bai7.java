package Buoi1;
import java.util.Scanner;

public class Bai7 {
    //Bài 7: Lập bảng cửu chương của số nguyên dương n (n<10)
    public static void main(String[] args) {
        int number;

        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();

        for(int i=0; i<10;i++)
        {
            System.out.println(number + " X " + i + " = " + i*number );
        }
    }
}
