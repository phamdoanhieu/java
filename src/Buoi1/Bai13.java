package Buoi1;

import java.util.Scanner;

public class Bai13 {
    //Bài 13: In ra màn hình hình vuông rỗng có cạnh n
    //* *	*	*	*
    //*	            *
    //*	            *
    //*	            *
    //* *	*	*	*
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Nhap n: ");
        int n = input.nextInt();
        for(int i=0; i<n;i++)
        {
            for (int j=0; j<n; j++)
            {
                if(i==0 || i==n-1)
                {
                    System.out.print("* ");

                }
                else
                {
                    if(j==0 || j==n-1)
                        System.out.print("* ");
                    else
                        System.out.print("  ");
                }
            }
            System.out.println("");
        }
    }
}
