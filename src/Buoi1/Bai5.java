package Buoi1;
import java.util.Scanner;

public class Bai5 {
    //Bài 5: Hãy kiểm tra số nguyên dương n có toàn chữ số lẻ/chẵn hay không?
    public static boolean chanle(int number) {
        int tmp,chanle;
        tmp=(number%10)%2;// tao 1 biến tạm lưu giá trị chẵn lẽ của số cuối
        while (number>0) {
            chanle = (number % 10) %2;

            number = number / 10;


            if (tmp != chanle) {
                return false;
            } else {
                tmp = chanle;
            }

        }
        return true;
    }
    public static void main(String[] args) {
        int number,chanle,tmp;
        int  min= 0,dem=0;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();
        if(chanle(number)){
            System.out.println(" Toan so chan/le");
        }
        else
        {
            System.out.println("Ko phai");
        }


    }
}
