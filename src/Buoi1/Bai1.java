package Buoi1;

import java.util.Scanner;

public class Bai1 {
    public static void main(String[] args) {
        int number,temp;
        int tong = 0;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();

        System.out.print("Tong cac chu so chan cua so nguyen duong " + number);
        while (number>0)
        {
            temp= number%10;
            number=number/10;
            if (temp%2==0)
            {
                tong+=temp;
            }
        }
        System.out.println(" la: "+tong);

    }
}
