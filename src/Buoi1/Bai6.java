package Buoi1;
import java.util.Scanner;

public class Bai6 {
    //Bài 6: Hãy kiểm tra các chữ số của số nguyên dương n có giảm dần từ trái sang phải hay không?
    public static boolean untitle(int number) {
        int tmp,chuso;
        tmp=number%10;// tao 1 biến tạm lưu giá trị chẵn lẽ của số cuối
        while (number>0) {
            chuso = (number % 10);
            number = number / 10;


            if (tmp > chuso) {
                return false;
            } else {
                tmp = chuso;
            }

        }
        return true;
    }
    public static void main(String[] args) {

        int number,chanle,tmp;
        int  min= 0,dem=0;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();
        if(untitle(number)){
            System.out.println(" giảm dần từ trái sang phải ");
        }
        else
        {
            System.out.println("Ko phai");
        }
    }
}
