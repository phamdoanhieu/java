package Buoi1;
import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class Bai10 {
    public static boolean SNT(int number)
    {
        for (int i=2; i<=sqrt(number); i++)
        {
            if(number%i==0)
                return  false;
        }
        return true;
    }
    public static void main(String[] args) {
        //Bài 10: Viết chương trình kiểm tra một số có phải số nguyên tố hay không.
        int number;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();
        if(SNT(number)){
            System.out.println("La SNT");
        }
        else
        {
            System.out.println("Ko la SNT");
        }
    }
}
