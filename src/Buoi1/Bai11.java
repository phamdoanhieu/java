package Buoi1;

public class Bai11 {
    public static void main(String[] args) {
        //Bài 11: Viết chương trình In ra tất cả các số lẻ nhỏ hơn 100 trừ các số 5, 7, 93.
        for(int i=0; i<100;i++)
        {
            if(i!= 5 && i!= 7 && i!=93 && i%2==1)
                System.out.println(i);
        }
    }
}
