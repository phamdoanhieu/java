package Buoi1;

import java.util.Scanner;

public class Bai14 {
    //    Bài 14: In ra màn hình tam giác vuông cân có cạnh n
//*
//        *	*
//        *	*	*
//        *	*	*	*
//        *	*	*	*	*
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Nhap n: ");
        int n = input.nextInt();
        for(int i=1; i<=n;i++)
        {
            for (int j=0; j<i; j++)
            {

                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
