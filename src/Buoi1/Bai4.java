package Buoi1;
import java.util.Scanner;

public class Bai4 {
    //Bài 4: Hãy đếm số lượng chữ số nhỏ nhất của số nguyên dương n.
    public  static int Min_of_Number(int number)
    {
        int chuso;
        int min=9;
        while (number>0)
        {
            chuso= number%10;
            number=number/10;
            min= (min>chuso) ? chuso : min;
        }
        return min;
    }
    public static void main(String[] args) {
        int number,chuso;
        int  min= 0,dem=0;
        Scanner input = new Scanner(System.in); //tao doi tuong input thuoc lop Scanner
        System.out.println("Nhap n: ");
        number = input.nextInt();


        System.out.print(" số lượng chữ số nhỏ nhất cua so nguyen duong " + number);
        min=Min_of_Number(number);

        while (number>0)
        {
            chuso= number%10;
            number=number/10;
            if (chuso==min)
            {
                dem++;
            }
        }
        System.out.println(" la: "+dem);

    }
}
