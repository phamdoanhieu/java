package Buoi1;

import java.util.Scanner;

public class Bai12 {
    //Bài 12: Viết chương trình nhập 3 số thực. Hãy thay tất cả các số âm bằng trị tuyệt đối của nó.
    public static float abs(float num)
    {
        return (num<0) ? (-num):num;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Nhap a b c: ");
        float a = input.nextFloat();
        float b = input.nextFloat();
        float c = input.nextFloat();
        a= abs(a);
        b= abs(b);
        c= abs(c);

        System.out.println("Output: "+ a);
        System.out.println("Output: "+ b);
        System.out.println("Output: "+ c);

    }
}
