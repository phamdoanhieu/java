package Buoi1;

import java.util.Scanner;

public class Bai15 {
    public static void main(String[] args) {
        //Bài 15: In ra màn hình tam giác cân có cạnh n
//     *
//    ***
//   *****
//  *******
// *********
        Scanner input = new Scanner(System.in);
        System.out.println("Nhap n: ");
        int n = input.nextInt();
        int i,j;
        for (i = 1; i <=n; i++)
        {
            for (j = 0; j < n-i; j++)
            {
                System.out.print(" ");
            }
            for (j=1; j <= 2*i-1; j++)
            {
                if(j==1 || j == 2*i-1)
                {
                    System.out.print("*");
                }
                else
                {
                    System.out.print(" ");
                }
            }

            System.out.println();
            if (i==n-1) /* đến hàng cuối cùng thì in ra cạnh đáy của tam giác*/
            {
                for(j=1; j<=2*n-1;j++)
                    System.out.print("*");
                break ;

            }
        }

    }
}

