package Buoi8.Bai1;

import java.util.HashMap;
import java.util.Scanner;

//Bài 1: Tạo từ điển anh - việt và thực hiện các chức năng sau:
//        1. Thêm từ vào từ điển.
//        2. Tra từ anh - việt. Nếu ko có thì tiến hành thêm từ vào từ điển.
//        3. Tra từ việt - anh. Nếu ko có thì tiến hành thêm từ vào từ điển.
//        4. Sắp xếp lại từ điển theo giá trị của key
//        5. Thoát
public class Main {
    public static void Menu()
    {
        System.out.println("Bài 1: Tạo từ điển anh - việt");
        System.out.println("1. Thêm từ vào từ điển.");
        System.out.println("2. Tra từ anh - việt. ");
        System.out.println("3. Tra từ việt - anh.");
        System.out.println("4. Sắp xếp lại từ điển theo giá trị của key");
        System.out.println("5. Thoát                                   ");
        System.out.println("Chọn: ");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Dictionary dictionary = new Dictionary();
        int chon;
// dịch viet -> eng;
        while (true)
        {
            Menu();
            chon = sc.nextInt();
            switch (chon)
            {
                case 1:
                    dictionary.Add_New_Word();
                    break;
                case 2:
                    dictionary.Search_En_Vi();
                    break;
                case 3:
                    dictionary.Search_Vi_En();
                    break;
                case 4:
                    dictionary.SortWithKey();
                    break;
                case 5:
                    return;

            }
        }


    }
}
