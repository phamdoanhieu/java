package Buoi8.Bai1;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class Dictionary {
    static Scanner sc = new Scanner(System.in);
    private HashMap<String, String> dictionary = new HashMap<>();

    public Dictionary(HashMap<String, String> dictionary) {
        this.dictionary = dictionary;
    }

    public Dictionary() {
    }

    public void Add_New_Word() {
            System.out.println("Nhập từ tiếng việt vào: ");
            String vi_word = sc.nextLine();
            System.out.println("Nhập từ tiếng Anh vào: ");
            String en_word = sc.nextLine();
            dictionary.put(vi_word, en_word);
    }

    public void XuatData() {
        dictionary.forEach((vi_word, en_word) ->
        {
            System.out.println(vi_word + " = " + en_word);
        });
    }

    //2. Tra từ anh - việt. Nếu ko có thì tiến hành thêm từ vào từ điển.
    public void Search_En_Vi() {
        System.out.println("Nhập từ tiếng anh muốn tra: ");
        String englishW = sc.nextLine();
        AtomicReference<Boolean> is_Existed = new AtomicReference<>(false);
        dictionary.forEach((vi_Word, en_Word) -> {
                    if (en_Word.equals(englishW)) {
                        System.out.println(en_Word + " : " + vi_Word);
                        is_Existed.set(true);
                    }

                }
        );
        if(!is_Existed.get())
        {
            Add_New_Word();
        }
    }
    public void Search_Vi_En() {
        System.out.println("Nhập từ tiếng việt muốn tra: ");
        String vietnameseW = sc.nextLine();
        AtomicReference<Boolean> is_Existed = new AtomicReference<>(false);
        dictionary.forEach((vi_Word, en_Word) -> {
                    if (vi_Word.equals(vietnameseW)) {
                        System.out.println(vi_Word + " : " + en_Word);
                        is_Existed.set(true);
                    }

                }
        );
        if(!is_Existed.get())
        {
            Add_New_Word();
        }
    }
//    4. Sắp xếp lại từ điển theo giá trị của key
        public void SortWithKey()
        {
            List<String> keys = new ArrayList<>(dictionary.keySet());
            Collections.sort(keys);
            keys.forEach(obj -> System.out.println(obj));
        }
//
//    for (String key : maps.keySet()) {
//        System.out.println("Key = " + key);
//    }
//        for (String value : maps.values()) {
//        System.out.println("Value = " + value );
//    }
}
