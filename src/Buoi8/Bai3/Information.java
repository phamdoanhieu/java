package Buoi8.Bai3;

import java.util.Scanner;

public class Information {
    private String lesson;
    private String subject;
    private String address;

    public Information(String lesson, String subject, String address) {
        this.lesson = lesson;
        this.subject = subject;
        this.address = address;
    }

    public Information() {
    }
    public void NhapData()
    {
        Scanner sc= new Scanner(System.in);
        System.out.println("Nhập ca học: ");
        lesson = sc.nextLine();
        System.out.println("Nhập môn học: ");
        subject = sc.nextLine();
        System.out.println("Nhập nơi học: ");
        address=sc.nextLine();
    }
    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Tiết: " + lesson + " học " + subject + " tại " + address ;
    }
}
