package Buoi8.Bai3;

import java.util.ArrayList;
import java.util.Collections;

public class ChiTiet1NgayHoc {
    ArrayList<Information> information = new ArrayList<>();
    public void Add()
    {
        Information tmp = new Information();
        tmp.NhapData();
        information.add(tmp);
    }
    public int getFistLesson(String str)
    {
        String[] newStr = str.split(",");
        String First = newStr[0];
        return Integer.parseInt(First);
    }
    public Boolean LessonCmp(String s1, String s2)
    {
        if(getFistLesson(s1)>getFistLesson(s2))
            return true;
        return false;
    }
    public void Sort_Lesson()
    {
        for(int i=0;i <information.size(); i++)
        {
            for (int j=i+1; j<information.size(); j++)
            {
                if(LessonCmp(information.get(i).getLesson(),information.get(j).getLesson()))
                {
                    Collections.swap(information,i,j);
                }
            }
        }
    }
    public void ShowData()
    {
        information.forEach(obj-> System.out.println(obj.toString()));
    }
    @Override
    public String toString() {
        return ""+information;
    }
}
