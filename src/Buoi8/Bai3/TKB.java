package Buoi8.Bai3;

import java.text.SimpleDateFormat;
import java.util.*;

public class TKB {
    HashMap<String, ChiTiet1NgayHoc> ngayHocHashMap = new HashMap<>();

    public void Add(String day)
    {
        ChiTiet1NgayHoc tmp = new ChiTiet1NgayHoc();
        tmp.Add();
        tmp.Add();
        ngayHocHashMap.put(day,tmp);
    }
    public void sortTKB()
    {
        Map<String,ChiTiet1NgayHoc> map = new TreeMap<>(ngayHocHashMap);
        map.forEach((key,val)-> {
            System.out.println("Hôm nay là: " + convertDate(key));//in ngày
            val.ShowData(); // hiển thị data của ngày
        }    )
        ;//inra
    }
    public void Sort_Lesson()
    {
        ngayHocHashMap.forEach((key,val) ->
        {
            val.Sort_Lesson();
        });
    }
    public String convertDate(String dateInMilliseconds)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE dd/MM/yyyy");
        String dateString = formatter.format(new Date(Long.parseLong(dateInMilliseconds)));
        dateString = dateString.replace("Monday", "Thứ 2 ngày");
        dateString = dateString.replace("Tuesday", "Thứ 3 ngày");
        dateString = dateString.replace("Wednesday", "Thứ 4 ngày");
        dateString = dateString.replace("Thursday", "Thứ 5 ngày");
        dateString = dateString.replace("Friđay", "Thứ 6 ngày");
        dateString = dateString.replace("Saturday", "Thứ 7 ngày");
        dateString = dateString.replace("Sunday", "Chủ Nhật ngày");
        return dateString;
    }

    @Override
    public String toString() {
        return "TKB{" +
                "ngayHocHashMap=" + ngayHocHashMap +
                '}';
    }
}
