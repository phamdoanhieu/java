package Buoi8.Bai2;

import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Data {
    private HashMap<String,String> data= new HashMap<String, String>();

    public Data(HashMap<String, String> data) {
        this.data = data;
    }

    public Data() {
        this.data.put("1572307200000","An toàn hệ điều hành"                    );
        this.data.put("1572912000000","Mã độc"                                  );
        this.data.put("1573516800000","Mật mã ứng dụng trong an toàn thông tin" );
        this.data.put("1572480000000","An toàn hệ điều hành"                   );
        this.data.put("1574294400000","Kỹ thuật vi xử lý"                       );
        this.data.put("1574726400000","Thực hành vật lý đại cương 1&2"          );
        this.data.put("1575331200000","Tiếng Anh 3"                             );
        this.data.put("1575936000000","Mật mã ứng dụng trong an toàn thông tin" );
        this.data.put("1576540800000","Thực hành vật lý đại cương 1&2"          );
        this.data.put("1577145600000","An toàn hệ điều hành"                    );
        this.data.put("1576022400000","Mã độc"                                  );
        this.data.put("1577232000000","An toàn hệ điều hành"                     );
        this.data.put("1565827200000","Cơ sở an toàn thông tin"                 );
        this.data.put("1566172800000","Kỹ thuật lập trình an toàn"              );
        this.data.put("1566432000000","Xây dựng ứng dụng web an toàn"           );
        this.data.put("1567036800000","Cơ sở an toàn thông tin"                 );
    }
    public String convertDate(String dateInMilliseconds)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(new Date(Long.parseLong(dateInMilliseconds)));
        return dateString;
    }
    //2. Đổi mili giây sang dạng ngày/tháng/năm (dd/m/yy) và hiển thị ra màn hình thông tin ngày giờ và môn học sau khi đã chuyển đổi.
    //VD: 24/10/2019 - "An toàn hệ điều hành"
    public void ConverAllToDate()
    {
        HashMap<String,String> newData = new HashMap<>();
        data.forEach((date,subject) ->
        {
            String newDate = convertDate(date);
            newData.put(newDate,subject);
        });
        System.out.println(newData);

    }
    public void SortWithKey()
    {
        Map<String, String> map = new TreeMap<>(data);

        List<String> keys =  new ArrayList<>(data.keySet());
        Collections.sort(keys);
        map.forEach((key,val)-> System.out.println(convertDate(key)+ " " +val));
    }
    @Override
    public String toString() {
        return "Data{"
                + data +
                '}';
    }
}
