package Buoi8.Bai2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void Menu()
    {
        System.out.println("1. Sắp xếp lại Map trên theo thứ tự thời gian tăng dần");
        System.out.println("2. Đổi mili giây sang dạng ngày/tháng/năm (dd/m/yy) và ");
        System.out.println("3. Thoat");
    }
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);

        Data data = new Data();
        int chon;
        while (true)
        {
            Menu();
            chon = sc.nextInt();
            switch (chon)
            {
                case 1:
                    data.SortWithKey();
                    break;
                case 2:
                    data.ConverAllToDate();
                    break;
                case 3:
                    return;
            }
        }
    }
}
