package Buoi8.lesson.Bai2;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // dịch viet -> eng;
        Scanner sc = new Scanner(System.in);
        HashMap<String,String> dictionary = new HashMap<>();
        //add key, value
        int i=0;
        do {
            System.out.println("Nhập từ tiếng việt vào: ");
            String vi_word = sc.nextLine();
            System.out.println("Nhập từ tiếng Anh vào: ");
            String en_word = sc.nextLine();
            dictionary.put(vi_word, en_word);
            i++;
        } while (i != 4);

        dictionary.forEach((vi_word,en_word)->
        {
            System.out.println(vi_word + " = " + en_word);
        });

    }
}
