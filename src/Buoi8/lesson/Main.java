package Buoi8.lesson;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<String,String> maps = new HashMap<>();
        //add key, value
        maps.put("Xin chao","hello");
        maps.put("Chao buoi sang","Good morning");

        maps.forEach((key, value) -> System.out.println(
                "Key = " + key + ", value = " + value));

        for (String key : maps.keySet()) {
            System.out.println("Key = " + key);
        }
        for (String value : maps.values()) {
            System.out.println("Value = " + value );
        }


    }
}
