package Buoi8.lesson.Bai1;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //thêm các từ vào từ điển
        // dịch viet -> eng;
        Scanner sc = new Scanner(System.in);
        HashMap<Integer,String> dictionary = new HashMap<>();
        //add key, value
        int i=0;
        do {
            System.out.println("Nhập từ vào: ");
            String word = sc.nextLine();
            dictionary.put(i++, word);

        } while (i != 4);

        dictionary.forEach((key, word) -> System.out.println(
                "STT = " + key + ", word = " + word));

    }
}
