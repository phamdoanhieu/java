package Buoi11.Bai1;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static JSONObject GetAllofJSONobj(JSONArray array)
    {
        JSONObject DataJSON = new JSONObject();
        for (int i = 0; i < array.size(); i++) {
            Map infor = (Map) array.get(i);
            Iterator<Map.Entry> itr1 = infor.entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                DataJSON.put(pair.getKey(),pair.getValue());
// hihi
            }
        }
        return DataJSON;
    }
    public static void JSONtoFile(JSONObject save,String fileName)
    {
        try {
            Formatter formatter = new Formatter(fileName);
            formatter.format("%s",save);
            formatter.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
    }
    public static void main(String[] args) throws IOException, ParseException {
        Scanner sc = new Scanner(System.in);
        String dir = "ex1.json";
        // Object obj = new JSONParser().parse(new FileReader(dir));
        JSONObject obj = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(dir)));

        // doc du lieu
        JSONObject user_data = (JSONObject) obj.get("user");
        String _id =(String)  user_data.get("_id");
        String username =(String)  user_data.get("username");
        String password =(String)  user_data.get("password");
        String role =(String)  user_data.get("role");
        String email =(String)  user_data.get("email");
        String access_token =(String)  user_data.get("access_token");
        String __v =(String)  user_data.get("__v");

        System.out.println("Các thông tin được lưu của user: ");
        System.out.println(user_data);

        //- Sửa lại địa chỉ email của người dùng, thêm thông tin về sđt. Sau đó lưu lại vào file ex1_profile.json
        System.out.println("Email cũ là: " + email);
        System.out.println("Nhập địa chỉ email mới: ");
        email = sc.nextLine();
        System.out.println("Thay đổi email thành công!");

        // tao obj json de luu
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("_id",_id);
        jsonObject.put("username",username);
        jsonObject.put("password",password);
        jsonObject.put("email",email);
        jsonObject.put("role",role);
        jsonObject.put("access_token",access_token);
        jsonObject.put("__v",__v);

        JSONtoFile(jsonObject, "ex1_profile.json");
    }
}

