package Buoi11.Bai2;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static JSONObject GetAllofJSONobj(JSONArray array)
    {
        JSONObject DataJSON = new JSONObject();
        for (int i = 0; i < array.size(); i++) {
            Map infor = (Map) array.get(i);
            Iterator<Map.Entry> itr1 = infor.entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                DataJSON.put(pair.getKey(),pair.getValue());
            }
        }
        return DataJSON;
    }
    public static void JSONtoFile(JSONObject save,String fileName)
    {
        try {
            Formatter formatter = new Formatter(fileName);
            formatter.format("%s",save);
            formatter.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
    }
    public static void main(String[] args) throws IOException, ParseException {
        Scanner sc = new Scanner(System.in);
        String dir = "ex2.json";
        // Object obj = new JSONParser().parse(new FileReader(dir));
        JSONObject obj = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(dir)));
        JSONArray student_list = (JSONArray) obj.get("student_list");

        //- In ra màn hình danh sách sinh viên có trong file json.
        for(int i=0; i<student_list.size();i++)
        {
            System.out.println(student_list.get(i));
        }

        //- Nhập vào từ bàn phím 1 mã sinh viên bất kì. Kiểm tra sinh viên đó có tồn tại trong danh sách không?

        System.out.println("Nhập mã sinh viên cần check: ");

        String studentId_key = sc.nextLine();
        //khai bao json de save data
        JSONObject saveData = new JSONObject();
        Boolean found = false;
        for (int i = 0; i < student_list.size(); i++) {
            Map saveInfo = (Map) student_list.get(i);
            System.out.println(saveInfo.get("_id"));
            if (studentId_key.equals(saveInfo.get("_id")))// nếu tìm thấy
            {
                saveData=GetAllofJSONobj((JSONArray) student_list.get(i));
                found = true;
                break;
            }
        }
        //+ Nếu có thì lưu thông tin sinh viên đó vào 1 file student.json

        if (found)
        {
            System.out.println("Tìm thấy sinh viên có id=" + studentId_key);
            JSONtoFile(saveData,"student.json");
        }
        //+ Nếu chưa có thì thêm thông tin của sinh viên đó và lưu vào file ex2_students.json
        else
        {

            System.out.println("không tìm thấy id, tiến hành thêm sinh viên: ");
            System.out.println("Nhập id:");
            String _id = sc.nextLine();
            System.out.println("Nhập username:");
            String username = sc.nextLine();
            System.out.println("Nhập email:");
            String email = sc.nextLine();

            saveData.put("_id",_id);
            saveData.put("username",username);
            saveData.put("email",email);

            student_list.add(saveData);
            JSONtoFile(saveData, "ex2.json");

        }
        System.out.println(saveData);


    }
}

