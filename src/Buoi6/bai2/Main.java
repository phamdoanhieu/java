package Buoi6.bai2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
//Bài 2: Đợt Trung Thu vừa rồi, CLB KIT có tổ chức 1 buổi ăn chơi cho các bạn trong CLB.
//        Vì quá nôn nóng muốn được chơi nên Mike cũng 1 số bạn đến buổi tiệc quá sớm.
//        Phải đợi lâu nên Mike rủ các bạn chơi bài tiến lên.
//        Sau khi chia bài, Mạnh cầm các lá bài của mình lên,
//        vì bài quá xấu nên tự nhiên Mike không biết sắp xếp lại các quân bài của mình.
//        Là 1 người bạn tốt của Mike, bạn hãy giúp Mike xếp lại bài.
//        Biết rằng J = 10, Q = 11, K = 12, A = 1 ( Lưu ý: Sắp xếp không phân biệt chất).

public class Main {
    public static void NhapMang_bai(ArrayList<Integer> dayBai)
    {
        Scanner sc = new Scanner(System.in);
        for(int i=0; i<13; i++)
        {
            System.out.println("Nhập quân bài thứ " + (i+1));
            String tmp = sc.nextLine();
            tmp= tmp.toUpperCase();
            if(tmp.equals("A"))
            {
                dayBai.add(1);
            } else
            if(tmp.equals("J"))
            {
                dayBai.add(11);
            } else
            if(tmp.equals("Q"))
            {
                dayBai.add(12);
            } else
            if(tmp.equals("K"))
            {
                dayBai.add(13);
            } else
                if (Integer.parseInt(tmp) >1 && Integer.parseInt(tmp) <11)
                     dayBai.add(Integer.valueOf(tmp));
        }
    }

    public static void InMang_bai(ArrayList<Integer> daySo)
    {
        Scanner sc = new Scanner(System.in);
        for (int i=0; i< daySo.size(); i++)
        {
            switch (daySo.get(i))
            {
                case 1:
                    System.out.print("A");
                    break;
                case 11:
                    System.out.print("J");
                    break;
                case 12:
                    System.out.print("Q");
                    break;
                case 13:
                    System.out.print("K");
                    break;
                default:
                    System.out.print(daySo.get(i));
            }
            System.out.print(" ");
        }
        System.out.println();
    }
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> dayBai = new ArrayList<>();
        NhapMang_bai(dayBai);
        Collections.sort(dayBai);
        InMang_bai(dayBai);
    }

}
