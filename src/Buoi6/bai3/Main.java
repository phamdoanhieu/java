package Buoi6.bai3;
//Bài 3: Nhập vào danh sách các từ có nghĩa (Ít nhất 10 từ). Sau đó thực hiện các chức năng sau:
//        1. Lọc ra các từ có ít nhất 3 từ trở lên.
//        2. Sắp xếp lại các từ Theo thứ tự A-Z
//        3. Nhập vào 1 từ bất kì. Kiểm tra xem từ ấy có trong danh sách hay không?

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void NhapMangString(ArrayList<String> arrString) {
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Nhập từ thứ "+ (i+1));
            arrString.add(sc.nextLine());
        }
    }

    public static void InMangString(ArrayList<String> arrString)
    {
        Scanner sc = new Scanner(System.in);
        for (int i=0; i< arrString.size(); i++)
        {
            System.out.println(i +". "+ arrString.get(i));
        }
    }
    public static void ChucNang1(ArrayList<String> arrString) //1. Lọc ra các từ có ít nhất 3 từ trở lên.
    {
        System.out.println("Các từ có ít nhất 3 từ trở lên: ");
        arrString.forEach((obj) ->
            {
                String list[] = obj.trim().split("\\s+");

                System.out.println(Arrays.toString(list));
            }
        );
//        for (int i=0; i< arrString.size(); i++)
//        {
//            if(arrString.get(i).length() >=3)
//                System.out.println(i +". "+ arrString.get(i));
//        }

    }
    public static void ChucNang2(ArrayList<String> arrString)//2. Sắp xếp lại các từ Theo thứ tự A-Z
    {
        Collections.sort(arrString);
        InMangString(arrString);
    }
    public static void ChucNang3(ArrayList<String> arrString)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập text cần check: ");
        String key = sc.nextLine();

        if(arrString.contains(key))
        {
            System.out.println("Có tồn tại \"" + key + "\" trong danh sách");
        }
        else
        {
            System.out.println("K tồn tại \"" + key + "\" trong danh sách");
        }
    }

    public static void Menu()
    {
        System.out.println("1. Lọc ra các từ có ít nhất 3 từ trở lên.         ");
        System.out.println("2. Sắp xếp lại các từ Theo thứ tự A-Z             ");
        System.out.println("3. Kiểm tra xem 1 từ có trong danh sách hay không?");
        System.out.println("4. Thoát");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> arrString = new ArrayList<>();
        NhapMangString(arrString);
        while (true)
        {
            Menu();
            int chon = sc.nextInt();
            switch (chon)
            {
                case 1:
                    ChucNang1(arrString);
                    break;
                case 2:
                    ChucNang2(arrString);
                    break;
                case 3:
                    ChucNang3(arrString);
                    break;
                case 4:
                    return;
            }
        }

    }
}
