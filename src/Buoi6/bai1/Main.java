package Buoi6.bai1;

import Buoi5.bai4.Xe;

import java.util.*;

public class Main {
    public static void NhapMang(ArrayList<Integer> daySo)
    {
        Scanner sc = new Scanner(System.in);
        int i=0;
        while (i++<9)
        {
            System.out.println("Nhập số: ");
            daySo.add(sc.nextInt());
            sc.nextLine();
            System.out.println("Bạn có muốn nhập tiếp k? Y/N");
            String key;
            key = sc.nextLine();
            if(key.equals("n"))
                break;
        }
    }
    public static void Menu()
    {
        System.out.println("1. Sắp xếp dãy Theo thứ tự tăng dần");
        System.out.println("2. Sắp xếp dãy Theo thứ tự giảm dần");
        System.out.println("3. Sắp xếp dãy 2 số chẵn , lẻ k đứng cạnh nhau");
        System.out.println("4. Kiếm tra số đó có tồn tại không.");
        System.out.println("5. Thoát");
    }

    public static void InMang(ArrayList<Integer> daySo)
    {
        Scanner sc = new Scanner(System.in);
        for (int i=0; i< daySo.size(); i++)
        {
            System.out.print(daySo.get(i)+ " ");
        }
        System.out.println();
    }
    public static void SapXepTangDan(ArrayList<Integer> daySo)
    {
        Collections.sort(daySo);
        InMang(daySo);
    }
    public static void SapXepGiamDan(ArrayList<Integer> daySo)
    {
        Collections.sort(daySo);
        for(int i=daySo.size()-1; i>=0; i--)
        {
            System.out.print(daySo.get(i)+ " ");
        }
    }
    public static void XenKe(ArrayList<Integer> daySo, ArrayList<Integer> daySoChan, ArrayList<Integer> daySoLe, ArrayList<Integer> DayMoi)
    {
        for(int i = 0; i<daySo.size(); i++)
        {
            if(daySo.get(i)%2==0)
                daySoChan.add(daySo.get(i));
            else
                daySoLe.add(daySo.get(i));
        }
        int k=0,size, dem_a=0,dem_b=0;
        size = (Math.min(daySoChan.size(), daySoLe.size()));
        while (k< size*2)
        {
            if( k%2==0)
            {
                DayMoi.add(daySoChan.get(dem_a++));
            }
            else
            {
                DayMoi.add(daySoLe.get(dem_b++));
            }
            k++;
        }
        // 1 3 5 7
        // 2 4 6 8 10 12
        // 2 1 4 3 6 5 6 7 10
        int tmp = 0;
        for(int i = k; i<daySo.size(); i++)
        {


            if(daySoChan.size() > daySoLe.size())
            {
                DayMoi.add(daySoChan.get(dem_a++));
                if(dem_a != daySoChan.size())
                {
                    DayMoi.add(0);

                }
            }
            else
            {

                DayMoi.add(daySoLe.get(dem_a++));
                if(dem_a != daySoLe.size())
                {
                    DayMoi.add(0);

                }

            }

        }
        InMang(DayMoi);
    }
    public static void KiemTraTonTai(ArrayList<Integer> daySo) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập number cần check: ");
        int n = sc.nextInt();
        int dem=0;
        for(int i=0; i<daySo.size(); i++)
        {
            if(daySo.get(i)== n)
            {
                dem++;
            }
        }
        if(daySo.contains(n))
        {
            System.out.println("Có tồn tại " + n + " trong mảng và xuất hiện " + dem + " lần");
        }
        else
        {
            System.out.println("K tồn tại " + n + " trong mảng");
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> daySo = new ArrayList<>();
        ArrayList<Integer> daySoChan = new ArrayList<>();
        ArrayList<Integer> daySoLe = new ArrayList<>();
        ArrayList<Integer> DayMoi = new ArrayList<>();
        NhapMang(daySo);
      while (true)
      {
          Menu();
          int chon = sc.nextInt();
          switch (chon)
          {
              case 1:
                  SapXepTangDan(daySo);
                  break;
              case 2:
                  SapXepGiamDan(daySo);
                  break;
              case 3:
                  XenKe(daySo,daySoChan,daySoLe,DayMoi);
                  break;
              case 4:
                  KiemTraTonTai(daySo);
                  break;
              case 5:
                  return;
          }
      }

    }


}
