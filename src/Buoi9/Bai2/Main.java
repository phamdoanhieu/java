package Buoi9.Bai2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws ParseException {
        //Bài 2: Phân tích đoạn json sau, đọc các thuộc tính:
        // id, sku, name, created_at, updated_at, product_links (Với thuộc tính created_at, updated_at chuyển về dạng dd/mm/yy),
        // sau đó tạo 1 đoạn json mới từ những thuộc tính trên
        String jsondata= "{\"id\":1989,\"sku\":\"1000255\",\"name\":\"Cà phê sáng tạo\",\"attribute_set_id\":25,\"price\":0,\"status\":1,\"visibility\":4,\"type_id\":\"bundle\",\"created_at\":\"2019-10-22 05:02:00\",\"updated_at\":\"2019-10-30 07:21:42\",\"weight\":0,\"product_links\":[{\"selection_id\":\"927\",\"qty\":\"1.0000\",\"canApplyMsrp\":false,\"can_change_quantity\":\"0\",\"optionId\":\"5\",\"priceType\":\"0\",\"name\":\"Cà phê G7 3in1 - Bịch 1 kg\",\"image\":\"https://trungnguyen.izysync.com/media/catalog/product/cache/958582f56d0bf2098317c8258f127df7/5/0/5000255-sang_tao_1-01.jpg\",\"prices\":{\"oldPrice\":{\"amount\":145700},\"basePrice\":{\"amount\":123845},\"finalPrice\":{\"amount\":123845}}}]}";
        Object object = new JSONParser().parse(jsondata);
        JSONObject jsonObject = (JSONObject) object;
        long id = (long) jsonObject.get("id");
        String sku =  (String) jsonObject.get("sku");
        String name =  (String) jsonObject.get("name");
        String created_at =  (String) jsonObject.get("created_at");
        String updated_at=  (String) jsonObject.get("updated_at");


        // iterating address Map
      //


        JSONObject parsed_Data = new JSONObject();
        parsed_Data.put("id",id);
        parsed_Data.put("sku",sku);
        parsed_Data.put("name",name);
        parsed_Data.put("created_at",created_at);
        parsed_Data.put("updated_at",updated_at);


        JSONArray jsonArray = (JSONArray) jsonObject.get("product_links");
        Map product_links = (Map)jsonArray.get(0);
        parsed_Data.put("product_links",product_links);
        System.out.println(parsed_Data);

        // tạo 1 Set có tên là product_links
        // chứa toàn bộ các entry (vừa key vừa value)
        // của product_links
        //Set<Map.Entry<String, String>> set_product_links = product_links.entrySet();


    }
}
