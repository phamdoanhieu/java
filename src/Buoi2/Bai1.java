package Buoi2;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Bai1 {

    public static void NhapMang(int a[])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            System.out.println("Array["+ i + "] =");

            a[i]= sc.nextInt();
        }
    }
    public static void XuatMang(int a[])
    {
        for (int i=0; i<a.length; i++)
        {
            System.out.println("a["+i+"] = " + a[i] );
        }
    }
    public static int boolean_to_int(boolean value)
    {
        if(value)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    public static void GiamDan(int a[])
    {
        for (int i=0; i<a.length;i++)
        {
            for(int j=i+1; j<a.length;j++)
            {
                switch (boolean_to_int(a[i]<a[j]))
                {
                    case 1:
                    {
                        int tmp = a[i];
                        a[i] = a[j];
                        a[j] = tmp;
                    }
                }
            }
        }
    }
    public static void TangDan(int a[])
    {
        for (int i=0; i<a.length;i++)
        {
            for(int j=i+1; j<a.length;j++)
            {
                switch (boolean_to_int(a[i]>a[j]))
                {
                    case 1: {
                        int tmp = a[i];
                        a[i] = a[j];
                        a[j] = tmp;
                    }
                }
            }
        }
    }
    public static void main(String[] args) {
        //1. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử.
        // Sắp xếp các phần tử theo thứ tự tăng dần, giảm dần ( Sử dụng switch...case).
        int n;
        System.out.println("Nhap n: ");
        Scanner sc = new Scanner(System.in);
        n=sc.nextInt();
        int[] a = new int[n];

        NhapMang(a);
        System.out.println("Mang Tang Dan da sap xep dung SWITCH CASE la: ");
        TangDan(a);
        XuatMang(a);
        System.out.println("Mang GIam dan da sap xep dung SWITCH CASE la:");
        GiamDan(a);
        XuatMang(a);

    }
}
