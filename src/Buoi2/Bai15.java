package Buoi2;

import java.util.Scanner;

public class Bai15 {
    public static void main(String[] args) {
//        15. Nhập vào từ bàn phím 1 chuỗi. Tạo menu với các chức năng sau:
//        - Viết hoa toàn bộ chuỗi
//        - Viết thường toàn bộ chuỗi
//        - Thoát
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap chuoi 1: ");
        String chuoi = sc.nextLine();
        int chon=0;
        while (true)
        {
            System.out.println("1. Viet hoa toan bo chuoi");
            System.out.println("2. Viet thuong toan bo chuoi");
            System.out.println("3. Thoat");
            System.out.println("Moi chon !");
            chon= sc.nextInt();
            switch (chon)
            {
                case 1:
                    String hoa = chuoi.toUpperCase();
                    System.out.println(hoa);
                    break;
                case 2:
                    String thuong=  chuoi.toLowerCase();
                    System.out.println(thuong);
                    break;
            }
            if(chon ==3)
            {
                System.out.println("Thoat Chuong Trinh!");
                break;
            }

        }

    }
}
