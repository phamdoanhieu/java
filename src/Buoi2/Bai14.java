package Buoi2;

import java.util.Scanner;

public class Bai14 {
    public static void main(String[] args) {
        //14. Nhập vào từ bàn phím 2 chuỗi. Tiến hành nối 2 chuỗi trên thành 1 chuỗi mới
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap chuoi 1: ");
        String chuoi1 = sc.nextLine();
        System.out.println("Nhap chuoi 2: ");
        String chuoi2 = sc.nextLine();

        System.out.println("Chuoi da ghep la : ");
        System.out.println(chuoi1.concat(chuoi2));
    }
}
