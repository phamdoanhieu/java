package Buoi2;

import java.util.Scanner;

public class Bai8 {
    public static void NhapMang(int a[])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            System.out.println("Array["+ i + "] =");

            a[i]= sc.nextInt();
        }
    }
    public static void XuatMang(int a[])
    {
        for (int i=0; i<a.length; i++)
        {
            System.out.println("a["+i+"] = " + a[i] );
        }
    }
    public static int Fibonaci(int n)
    {
        if(n==0)
            return 0;
        if(n==1)
            return 1;
        return Fibonaci(n-1) + Fibonaci(n-2);
    }

    public static void main(String[] args) {
        //1. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử.
        // Sắp xếp các phần tử theo thứ tự tăng dần, giảm dần ( Sử dụng switch...case).
        int n;
        System.out.println("Nhap n: ");
        Scanner sc = new Scanner(System.in);
        n=sc.nextInt();
        int[] a = new int[n];

//        NhapMang(a);
        for (int i=0; i<n;i++)
        {
            a[i]=Fibonaci(i);
        }
        XuatMang(a);
    }
}