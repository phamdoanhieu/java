package Buoi2;


import java.util.Arrays;
import java.util.Scanner;

public class Bai6 {
    //    6. Sử dụng mảng 1 chiều nhập vào dãy số nguyên dương gồm n phần tử. Sắp xếp lại dãy số theo dạng, nửa đầu của dãy là các số nguyên dương theo thứ tự tăng dần, nửa sau của dãy là các số nguyên âm sắp xếp theo thứ tự giảm dần.
//            INPUT: -3, 4, 6, -5, 3, 8, -2, -1
//    OUTPUT: 3, 4, 6, 8, -1, -2, -3, -5
    public static void NhapMang(int a[])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            System.out.println("Array["+ i + "] =");

            a[i]= sc.nextInt();
        }
    }
    public static void XuatMang(int a[])
    {
        for (int i=0; i<a.length; i++)
        {
            System.out.println("a["+i+"] = " + a[i] );
        }
    }
    public static void GiamDan(int a[], int n)
    {
        for (int i=0; i<n;i++)
        {
            for(int j=i+1; j<n;j++)
            {
                if(a[i]<a[j])
                {
                    int tmp = a[i];
                    a[i]=a[j];
                    a[j]=tmp;
                }
            }
        }
    }
    public static void main(String[] args) {
        //1. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử.
        // Sắp xếp các phần tử theo thứ tự tăng dần, giảm dần ( Sử dụng switch...case).
        int n;
        System.out.println("Nhap n: ");
        Scanner sc = new Scanner(System.in);
        n=sc.nextInt();
        int[] a = new int[n];
        int flags = 0;

        NhapMang(a);
        GiamDan(a,n);
        for(int i=0; i<a.length; i++)
        {
            if(a[i]<0) {
                flags = i;
                break;
            }
        }
        System.out.println(flags);
        Arrays.sort(a,0,flags);

        XuatMang(a);
    }
}
