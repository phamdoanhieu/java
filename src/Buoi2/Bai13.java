package Buoi2;

import java.util.Scanner;

public class Bai13 {
    public static void main(String[] args) {
        //13. Nhập và từ bàn phím 1 chuỗi và 1 kí tự bất kì.
        // Kiểm tra kí tự vừa nhập vào có thuộc chuỗi hay không? Và kí tự đó xuất hiện mấy lần trong chuỗi
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap chuoi : ");
        String chuoi = sc.nextLine();
        System.out.println("Nhap vao 1 ki tu : ");
        char key = sc.next().charAt(0);
        int Dem=0;
        for (int i=0; i<chuoi.length(); i++)
        {
            if(chuoi.charAt(i) == key)
                Dem++;
        }
        if(Dem>0)
        {
            System.out.println(key+ " co thuoc chuoi da cho");
            System.out.println("Xuat hien so lan la: " + Dem);
        }
    }
}
