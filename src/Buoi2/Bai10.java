package Buoi2;

import java.util.Arrays;
import java.util.Scanner;

public class Bai10 {

    static void SapXepMang2ChieuTangDan(int a[][], int SoDong, int SoCot)
    {
        int n = 0;
        int b[] = new int[SoCot*SoDong];
        for(int i = 0; i < SoDong; i++)
        {
            for(int j = 0; j < SoCot; j++)
            {
                b[n] = a[i][j];
                n++;
            }
        }
        Arrays.sort(b);
        n = 0;
        for(int i = 0; i < SoDong; i++)
        {
            for(int j = 0; j < SoCot; j++)
            {
                a[i][j] = b[n];
                n++;
            }
        }
    }
    public static void NhapMang(int a[][])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {
                System.out.println("Array["+ i + "]["+j+"] =");
                a[i][j]= sc.nextInt();
            }
        }
    }
    public static void XuatMang(int a[][])
    {
        for (int i=0; i<a.length; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {

                System.out.println("a["+i+"]["+j+"] = " + a[i][j] );
            }

        }
    }
    public static void main(String[] args) {
        int n=3;
        int[][] a= new int[n][n];
        Scanner sc = new Scanner(System.in);
        System.out.println(a[0].length);
        NhapMang(a);
        SapXepMang2ChieuTangDan(a,3,3);
        XuatMang(a);
    }
}
