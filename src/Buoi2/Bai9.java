package Buoi2;

import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class Bai9 {
    public static void NhapMang(int a[][])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {

                System.out.println("Array["+ i + "]["+j+"] =");
                a[i][j]= sc.nextInt();
            }
        }
    }
    public static void XuatMang(int a[][])
    {
        for (int i=0; i<a.length; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {

                System.out.println("a["+i+"]["+j+"] = " + a[i][j] );
            }

        }
    }
    public static boolean SNT(int n)
    {
        for(int i=2; i<=sqrt(n);i++)
        {
            if(n%i==0)
            {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        int n;

        int[][] a = new int[3][4];

        NhapMang(a);
        for (int i=0; i<a.length; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {
                if(SNT(a[i][j]))
                {
                    System.out.println(a[i][j]);
                }

            }

        }

    }
}
