package Buoi2;

import java.util.Scanner;

public class Bai2 {
    public static void NhapMang(int a[])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            System.out.println("Array["+ i + "] =");

            a[i]= sc.nextInt();
        }
    }
    public static void XuatMang(int a[])
    {
        for (int i=0; i<a.length; i++)
        {
            System.out.println("a["+i+"] = " + a[i] );
        }
    }
    public static int Sum(int a[])
    {
        int Sum=0;
        for (int i=0; i<a.length; i++)
        {
            Sum+=a[i];
        }
        return Sum;
    }
    public static void main(String[] args) {

        //1. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử.
        // Sắp xếp các phần tử theo thứ tự tăng dần, giảm dần ( Sử dụng switch...case).
        int n;
        System.out.println("Nhap n: ");
        Scanner sc = new Scanner(System.in);
        n=sc.nextInt();
        int[] a = new int[n];
        int Tong=0;
        NhapMang(a);
        Tong=Sum(a);
        System.out.println("Tong cua Arr = " + Tong);
    }
}
