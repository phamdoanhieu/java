package Buoi2;

import java.util.Scanner;

public class Bai12 {

    public static void NhapMang(int a[][])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {
                System.out.println("Array["+ i + "]["+j+"] =");
                a[i][j]= sc.nextInt();
            }
        }
    }
    public static void XuatMang(int a[][])
    {
        for (int i=0; i<a.length; i++)
        {
            for(int j= 0; j <a[i].length; j++)
            {


                System.out.println("a["+i+"]["+j+"] = " + a[i][j] );
            }

        }
    }
    public static void NhanMang(int a[][], int b[][], int c[][], int hang1, int cot1, int cot2)
    {
        for (int i=0;i<hang1;i++)
        {
            for (int j=0;j<cot2; j++)
            {
                int Sum = 0;
                for (int k=0; k<cot1; k++)
                {
                    Sum+=a[i][k]*b[k][j];
                }
                c[i][j]=Sum;
            }
        }
    }
    public static boolean MangGiongNhau(int a[][], int b[][])
    {
        for (int i=0; i<a.length; i++)
        {
            for (int j=0; j<a.length; j++)
            {
                if(a[i][j]!= b[i][j])
                    return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int hang1, hang2, cot1, cot2;
        System.out.println("Nhap so hang cua Ma tran A");
        hang1 = sc.nextInt();
        System.out.println("so cot");
        cot1 = sc.nextInt();
        System.out.println("Nhap so hang cua Ma tran B");
        hang2 = sc.nextInt();
        System.out.println("So cot");
        cot2 = sc.nextInt();
        int[][] a= new int [hang1][cot1];
        int[][] b= new int [hang2][cot2];
        int[][] c = new int [hang1][cot2];
        int[][] d = new int [hang1][cot2];
        if(hang2 != cot1)
        {
            System.out.println("Ma tran nay ko nhan duoc!");
        }
        else
        {
            NhapMang(a);
            NhapMang(b);
            NhanMang(a,b,c,hang1,cot1,cot2);
            NhanMang(b,a,d,hang1,cot1,cot2);
            if(MangGiongNhau(c,d))
            {
                System.out.println("B la ma tran nghich dao cua A");
            }
            else
            {
                System.out.println("B Khong la ma tran nghich dao cua A");
            }

        }
    }
}


