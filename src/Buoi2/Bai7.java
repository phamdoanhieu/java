package Buoi2;

import java.util.Arrays;
import java.util.Scanner;

public class Bai7 {
    public static void NhapMang(int a[])
    {
        Scanner sc=new Scanner(System.in);
        for (int i = 0; i <a.length ; i++)
        {
            System.out.println("Array["+ i + "] =");

            a[i]= sc.nextInt();
        }
    }
    public static void XuatMang(int a[])
    {
        for (int i=0; i<a.length; i++)
        {
            System.out.println("a["+i+"] = " + a[i] );
        }
    }
    public static int FindMax(int a[])
    {
        int max=0;
        for(int i=0;i<a.length;i++)
        {
            if(a[max]<a[i])
                max=i;
        }
        return max;
    }
    public static int FindMin(int a[])
    {
        int min=0;
        for(int i=0;i<a.length;i++)
        {
            if(a[min]>a[i])
                min=i;
        }
        return min;
    }
    public static void main(String[] args) {

        int n;
        System.out.println("Nhap n: ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int[] a = new int[n];

        NhapMang(a);
        System.out.println("A[Max]= A["+FindMax(a)+"] = " +a[FindMax(a)]);
        System.out.println("A[Min]= A["+FindMin(a)+"] = " +a[FindMin(a)]);
    }
}
