package Buoi10.Bai1;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Formatter;
import java.util.Map;
import java.util.Scanner;
//   Bài 1: Cho file json ex1.json . N
//   hập vào từ bàn phím email và password.
//   Nếu trùng với email và password trong file thì tiến hành khai báo các thông tin cá nhân bao gồm:
//   Họ tên, số điện thoại, địa chỉ, quê quán, năm sinh.
//   Sau đó lưu lại thông tin cá nhân vào 1 file profile.json
//   Bài 2: Cho file json ex2.json. Nhập vào từ bàn phím mã lớp, nếu mã lớp tồn tại lưu thông tin của lớp đó vào 1 file classroom.json.

public class Bai1 {
    public static void main(String[] args) throws IOException, ParseException {
       // C:\Users\Admin\OneDrive\Documents\Kit\java\src\Buoi10
        String dir = "C:\\Users\\Admin\\OneDrive\\Documents\\Kit\\java\\src\\Buoi10\\ex1.json";
        // Object obj = new JSONParser().parse(new FileReader(dir));
        JSONObject obj = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(dir)));
        String k_Email = (String) obj.get("email");
        String k_Password = (String) obj.get("password");
        String fullName = null,phoneNumber,address, nativeCountry;
        int yearOfBirth;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập email: ");
        String user_Email = sc.nextLine();
        System.out.println("Nhập password: ");
        String user_Password = sc.nextLine();
        // Nếu trùng với email và password trong file thì tiến hành khai báo các thông tin
        if(user_Email.equals(k_Email) && user_Password.equals(k_Password))
        {
//   Họ tên, , , , .
            System.out.println("Nhập họ tên: ");
            fullName=sc.nextLine();
            System.out.println("Nhập số điện thoại: ");
            phoneNumber=sc.nextLine();
            System.out.println("Nhập địa chỉ: ");
            address=sc.nextLine();
            System.out.println("Nhập quê quán: ");
            nativeCountry=sc.nextLine();
            System.out.println("Nhập năm sinh: ");
            yearOfBirth=sc.nextInt();
            // ghi data
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fullName",fullName);
            jsonObject.put("phoneNumber",phoneNumber);
            jsonObject.put("address",address);
            jsonObject.put("nativeCountry",nativeCountry);
            jsonObject.put("yearOfBirth",yearOfBirth);

             //ghi file
        try {
            Formatter formatter = new Formatter("profile.json");
            formatter.format("%s",jsonObject);
            formatter.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
        }
        else
        {
            System.out.println("Email/Password bị sai! ");
        }

        //chuyển dạng obj sang JSON OBJ
        System.out.println(obj);
    }
}
