package Buoi10.Bai2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

//Bài 2: Cho file json ex2.json. Nhập vào từ bàn phím mã lớp,
// nếu mã lớp tồn tại lưu thông tin của lớp đó vào 1 file classroom.json.
public class Main {
    public static JSONObject GetAllofJSONobj(JSONArray array)
    {
        JSONObject DataJSON = new JSONObject();
        for (int i = 0; i < array.size(); i++) {
            Map infor = (Map) array.get(i);
            Iterator<Map.Entry> itr1 = infor.entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                DataJSON.put(pair.getKey(),pair.getValue());
            }
        }
        return DataJSON;
    }
    public static void JSONtoFile(JSONObject save,String fileName)
    {
        try {
            Formatter formatter = new Formatter(fileName);
            formatter.format("%s",save);
            formatter.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e);
        }
    }
    public static void main(String[] args) throws IOException, ParseException {
        Scanner sc = new Scanner(System.in);
        String dir = "C:\\Users\\Admin\\OneDrive\\Documents\\Kit\\java\\src\\Buoi10\\ex2.json";
        // Object obj = new JSONParser().parse(new FileReader(dir));
        JSONObject obj = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(dir)));
        JSONArray classsroomList = (JSONArray) obj.get("classroom_list");

        System.out.println("Nhập mã lớp cần check: ");

        String roomCode_key = sc.nextLine();
        JSONObject saveData = new JSONObject();
        // lấy mã lớp, data
        Boolean found = false;
        for (int i = 0; i < classsroomList.size(); i++) {
            Map saveInfo = (Map) classsroomList.get(i);
            System.out.println(saveInfo.get("code"));
            if (roomCode_key.equals(saveInfo.get("code")))// nếu tìm thấy
            {
                saveData=GetAllofJSONobj(classsroomList);
                found = true;
                break;
            }
        }
        if (found)
        {
            System.out.println("Tìm thấy lớp có mã: " + roomCode_key);
            JSONtoFile(saveData,"classroom.json");
        }
        else
        {
            System.out.println("không tìm thấy");
        }
        System.out.println(saveData);
       /*   JSONObject h_test = new JSONObject();
            for (int i = 0; i < classsroomList.size(); i++) {
            Map infor = (Map) classsroomList.get(i);
            Iterator<Map.Entry> itr1 = infor.entrySet().iterator();
            while (itr1.hasNext()) {
                Map.Entry pair = itr1.next();
                h_test.put(pair.getKey(),pair.getValue());
            }
        }

        System.out.println(h_test);*/

    }
}
