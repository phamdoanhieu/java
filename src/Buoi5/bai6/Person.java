package Buoi5.bai6;

import java.util.Scanner;

//- Lớp Person: bao gồm các thuộc tính họ tên, địa chỉ, phương thức toString.
public class Person {
    Scanner sc = new Scanner(System.in);
    protected String HoTen;
    protected String DiaChi;

    // constructor

    public Person(String hoTen, String diaChi) {
        HoTen = hoTen;
        DiaChi = diaChi;
    }
    public Person() {
        HoTen = "";
        DiaChi = "";
    }

    // geter and set


    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String hoTen) {
        HoTen = hoTen;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String diaChi) {
        DiaChi = diaChi;
    }

//nhập
    public void Nhap()
    {
        System.out.println("Nhập họ tên: ");
        HoTen = sc.nextLine();
        System.out.println("Nhập địa chỉ: ");
        DiaChi = sc.nextLine();
    }
    @Override
    public String toString() {
        return "Họ tên: " + HoTen +", Địa chỉ: " + DiaChi;
    }
}
