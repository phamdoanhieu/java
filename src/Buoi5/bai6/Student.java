package Buoi5.bai6;
//o Lớp Student: bao gồm các thuộc tính
// điểm môn học 1, điểm môn học 2,
// và các phương thức: tính điểm TB, đánh giá,
// overriding phương thức toString trả về bảng điểm sinh viên (gồm thông tin thuộc tính và điểm TB).
public class Student extends Person {

    private float DiemMonHoc1;
    private float DiemMonHoc2;

    // constructor

    public Student(String hoTen, String diaChi, float diemMonHoc1, float diemMonHoc2) {
        super(hoTen, diaChi);
        DiemMonHoc1 = diemMonHoc1;
        DiemMonHoc2 = diemMonHoc2;
    }

    public Student() {
        DiemMonHoc1 = -1;
        DiemMonHoc2 = -1;
    }

    // get and set

    public float getDiemMonHoc1() {
        return DiemMonHoc1;
    }

    public void setDiemMonHoc1(float diemMonHoc1) {
        DiemMonHoc1 = diemMonHoc1;
    }

    public float getDiemMonHoc2() {
        return DiemMonHoc2;
    }

    public void setDiemMonHoc2(float diemMonHoc2) {
        DiemMonHoc2 = diemMonHoc2;
    }

    // Hàm
    public float DiemTB()
    {
        return  (DiemMonHoc1+DiemMonHoc2)/2;
    }
    public String  DanhGia()
    {
        if(DiemTB()>= 0 && DiemTB()<5)
            return "Yếu";
        if(DiemTB() >= 5 && DiemTB() <6.5 )
            return "Trung Bình";
        if(DiemTB()>=6.5 && DiemTB() <8)
            return "Khá";
        if(DiemTB() >=8 && DiemTB() <=10)
            return "Giỏi";
        return "Điểm Không Hợp Lệ";
    }


    // nhập
    @Override
    public void Nhap()
    {
        super.Nhap();
        System.out.println("Nhập điểm môn học thứ 1: ");
        DiemMonHoc1 = sc.nextFloat();
        System.out.println("Nhập điểm môn học thứ 2: ");
        DiemMonHoc2 = sc.nextFloat();

    }
    @Override
    public String toString() {
        return "[Student: " + super.toString() + ", Điểm môn học 1: " + DiemMonHoc1 + ", Điểm môn học 2: " + DiemMonHoc2 +
                ", Điểm trung bình: " + DiemTB() + ", Xếp loại: " + DanhGia() + "]";
    }
}
