package Buoi5.bai6;

public class Employee extends Person {
    private float HeSoLuong;
    private float LuongCoBan;

    // construct
    public Employee(String hoTen, String diaChi, float heSoLuong) {
        super(hoTen, diaChi);
        HeSoLuong = heSoLuong;
        LuongCoBan = 1000000;
    }

    public Employee() {
        HeSoLuong = 0;
        LuongCoBan = 1000000;
    }

    //get and set
    public float getHeSoLuong() {
        return HeSoLuong;
    }

    public void setHeSoLuong(float heSoLuong) {
        HeSoLuong = heSoLuong;
    }

    public float getLuongCoBan() {
        return LuongCoBan;
    }

    public void setLuongCoBan(float luongCoBan) {
        LuongCoBan = luongCoBan;
    }

    @Override
    public void Nhap() {
        super.Nhap();
        System.out.println("Nhập hệ số lương: ");
        HeSoLuong = sc.nextFloat();
    }

    @Override
    public String toString() {
        return "[Employee: " + super.toString() + ", Tiền lương: " + HeSoLuong * LuongCoBan + "]";
    }
}
