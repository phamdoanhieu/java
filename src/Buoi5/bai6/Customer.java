package Buoi5.bai6;

import Buoi5.bai5.GiaoDich;

public class Customer extends Person {
    private String TenCongTy;
    private float GiaTriHoaDon;
    private String DanhGia;

    public Customer(String hoTen, String diaChi, String tenCongTy, float giaTriHoaDon, String danhGia) {
        super(hoTen, diaChi);
        TenCongTy = tenCongTy;
        GiaTriHoaDon = giaTriHoaDon;
        DanhGia = danhGia;
    }

    public Customer() {
        super();
        TenCongTy = "";
        GiaTriHoaDon = 0;
        DanhGia = "";
    }

    public String getTenCongTy() {
        return TenCongTy;
    }

    public void setTenCongTy(String tenCongTy) {
        TenCongTy = tenCongTy;
    }

    public float getGiaTriHoaDon() {
        return GiaTriHoaDon;
    }

    public void setGiaTriHoaDon(float giaTriHoaDon) {
        GiaTriHoaDon = giaTriHoaDon;
    }

    public String getDanhGia() {
        return DanhGia;
    }

    public void setDanhGia(String danhGia) {
        DanhGia = danhGia;
    }
    @Override
    public void Nhap() {
        super.Nhap();
        System.out.println("Nhập tên công ty: ");
        TenCongTy = sc.nextLine();
        System.out.println("Nhập giá trị hóa đơn: ");
        GiaTriHoaDon = sc.nextInt();
        System.out.println("Nhập Đánh giá ");
        DanhGia = sc.nextLine();
    }


    @Override
    public String toString() {
        return "[Customer: " + super.toString() +", Tên Công Ty: " + TenCongTy
                + ", Giá trị hóa đơn: "+ GiaTriHoaDon + ", Đánh giá: " + DanhGia + "]";
    }
}
