package Buoi5.bai4;
//- Chuyến xe nội thành: Mã số chuyến, Họ tên tài xế, số xe, số tuyến, số km đi được, doanh thu.

import java.util.Scanner;

public class XeNoiThanh extends Xe {
    private int SoTuyen;
    private float km;
    private int DoanhThu;

    public XeNoiThanh(String maSo, String tenTaiXe, String bienSoXe, int soTuyen, float km, int doanhThu) {
        super(maSo, tenTaiXe, bienSoXe);
        SoTuyen = soTuyen;
        this.km = km;
        DoanhThu = doanhThu;
    }

    public XeNoiThanh(String maSo, String tenTaiXe, String bienSoXe) {
        super(maSo, tenTaiXe, bienSoXe);
    }

    @Override
    public void Nhap()
    {
        Scanner sc = new Scanner(System.in);
        super.Nhap();
        System.out.println("Nhập Số tuyến: ");
        this.SoTuyen = sc.nextInt();
        System.out.println("Nhập Số km: ");
        this.km = sc.nextFloat();
        System.out.println("Nhập Doanh thu: ");
        this.DoanhThu = sc.nextInt();

    }

    @Override
    public void Xuat()
    {
        super.Xuat();
        System.out.println("Số tuyến: "+ this.SoTuyen);
        System.out.println("Số km: "+ this.km);
        System.out.println("Doanh Thu: " + this.DoanhThu);
    }

    public int getSoTuyen() {
        return SoTuyen;
    }

    public void setSoTuyen(int soTuyen) {
        SoTuyen = soTuyen;
    }

    public float getKm() {
        return km;
    }

    public void setKm(float km) {
        this.km = km;
    }

    public int getDoanhThu() {
        return DoanhThu;
    }

    public void setDoanhThu(int doanhThu) {
        DoanhThu = doanhThu;
    }
}
