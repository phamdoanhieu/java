package Buoi5.bai4;

import java.util.Scanner;

//Bài 4: Công ty du lịch V quản lý thông tin là các chuyến xe. Thông tin của 2 loại chuyến xe:
//        - Chuyến xe nội thành: Mã số chuyến, Họ tên tài xế, số xe, số tuyến, số km đi được, doanh thu.
//        - Chuyến xe ngoại thành: Mã số chuyến, Họ tên tài xế, số xe, nơi đến, số ngày đi được, doanh thu.
//        Thực hiện các yêu cầu sau:
//        - Xây dựng các lớp với chức năng thừa kế.
//        - Viết chương trình quản lý các chuyến xe theo dạng cây thừa kế với các phương thức sau:
//        1. Nhập, xuất danh sách các chuyến xe (danh sách có thể dùng cấu trúc mảng).
//        2. Tính tổng doanh thu cho từng loại xe.
public class Xe {
    protected String MaSo;
    protected String TenTaiXe;
    protected String BienSoXe;

    public Xe(String maSo, String tenTaiXe, String bienSoXe) {
        MaSo = maSo;
        TenTaiXe = tenTaiXe;
        BienSoXe = bienSoXe;
    }

    public void Nhap()
    {
        Scanner sc= new Scanner(System.in);
        System.out.println("Nhập Tên tài xế: ");
        this.TenTaiXe = sc.nextLine();
        System.out.println("Biển số xe: ");
        this.BienSoXe = sc.nextLine();
    }
    public void Xuat()
    {
        System.out.println("Mã số xe: " + this.MaSo);
        System.out.println("Tên tài xế: " + this.TenTaiXe );
        System.out.println("Biển số xe: " + this.BienSoXe);
    }

    public String getMaSo() {
        return MaSo;
    }

    public void setMaSo(String maSo) {
        MaSo = maSo;
    }

    public String getTenTaiXe() {
        return TenTaiXe;
    }

    public void setTenTaiXe(String tenTaiXe) {
        TenTaiXe = tenTaiXe;
    }

    public String getBienSoXe() {
        return BienSoXe;
    }

    public void setBienSoXe(String bienSoXe) {
        BienSoXe = bienSoXe;
    }
}
