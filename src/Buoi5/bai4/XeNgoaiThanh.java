package Buoi5.bai4;

import java.util.Scanner;

//- Chuyến xe ngoại thành: Mã số chuyến, Họ tên tài xế, số xe, nơi đến, số ngày đi được, doanh thu.
public class XeNgoaiThanh extends Xe {
    private String NoiDen;
    private int SoNgayDi;
    private int DoanhThu;


    public XeNgoaiThanh(String maSo, String tenTaiXe, String bienSoXe, String noiDen, int soNgayDi, int doanhThu) {
        super(maSo, tenTaiXe, bienSoXe);
        NoiDen = noiDen;
        SoNgayDi = soNgayDi;
        DoanhThu = doanhThu;
    }

    public XeNgoaiThanh(String maSo, String tenTaiXe, String bienSoXe) {
        super(maSo, tenTaiXe, bienSoXe);
    }

    @Override
    public void Nhap()
    {
        Scanner sc = new Scanner(System.in);
        super.Nhap();
        System.out.println("Nhập Nơi đến: ");
        this.NoiDen = sc.nextLine();
        System.out.println("Nhập Doanh Thu: ");
        this.DoanhThu = sc.nextInt();
    }

    @Override
    public void Xuat()
    {
        super.Xuat();
        System.out.println("Nơi đến: "+ this.NoiDen);
        System.out.println("Doanh Thu: " + this.DoanhThu);
    }
    public String getNoiDen() {
        return NoiDen;
    }

    public void setNoiDen(String noiDen) {
        NoiDen = noiDen;
    }

    public int getSoNgayDi() {
        return SoNgayDi;
    }

    public void setSoNgayDi(int soNgayDi) {
        SoNgayDi = soNgayDi;
    }

    public int getDoanhThu() {
        return DoanhThu;
    }

    public void setDoanhThu(int doanhThu) {
        DoanhThu = doanhThu;
    }
}
