package Buoi5.bai1;

import java.awt.*;
import java.util.Scanner;

public class Vehicle {
    private String name;
    private int giatri;
    private int dungtich;

    public Vehicle() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGiatri() {
        return giatri;
    }

    public void setGiatri(int giatri) {
        this.giatri = giatri;
    }

    public int getDungtich() {
        return dungtich;
    }

    public void setDungtich(int dungtich) {
        this.dungtich = dungtich;
    }

    public Vehicle(String name, int giatri, int dungtich) {
        this.name = name;
        this.giatri = giatri;
        this.dungtich = dungtich;
    }

    public int Thue()
    {
        int thue= this.giatri;
        if(this.dungtich<100)
        {
            thue += this.giatri/100;
        }
        else if(this.dungtich>=100 && this.dungtich <=200)
        {
            thue += this.giatri/100*3;
        }
        else if(this.dungtich>200)
        {
            thue+= this.giatri/100*5;
        }

        return thue;
    }
//1. Nhập thông tin và tạo các đối tượng carOne, carTwo, carThree
//2. Xuất bảng kê khai tiền thuế trước bạ của các xe.
//3. Thoát.
//
    public static void Bang_Thue(Vehicle carOne, Vehicle carTwo, Vehicle carThree)
    {
        int i=1;
        System.out.printf("Bang kê khai tiền thuế\n");
        System.out.printf("|%3s|%-10s|%10s|\n","STT","Tên xe", "Thuế");
        System.out.printf("|%3d|%-10s|%10d|\n",i++,carOne.name,carOne.Thue());
        System.out.printf("|%3d|%-10s|%10d|\n",i++,carTwo.name,carTwo.Thue());
        System.out.printf("|%3d|%-101s|%10d|\n",i++,carThree.name,carThree.Thue());

    }
    public static void Menu()
    {
        System.out.print("1. Nhập thông tin và tạo các đối tượng carOne, carTwo, carThree\n");
        System.out.print("2. Xuất bảng kê khai tiền thuế trước bạ của các xe.\n");
        System.out.print("3. Thoát.\n");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Vehicle carOne =new Vehicle();
        Vehicle carTwo=new Vehicle();
        Vehicle carThree=new Vehicle();
        int chon;
        while (true)
        {
            Menu();
            chon = sc.nextInt();

            sc.nextLine();//fflush
            switch (chon)
            {
                case 1:
                {
                    System.out.println("Nhập giá trị và dung tích xe 1");
                    carOne = new Vehicle("Car One",sc.nextInt(),sc.nextInt());
                    System.out.println("Nhập giá trị và dung tích xe 2");
                    carTwo = new Vehicle("Car Two", sc.nextInt(),sc.nextInt());
                    System.out.println("Nhập giá trị và dung tích xe 3");
                    carThree = new Vehicle("Car Three", sc.nextInt(),sc.nextInt());
                    break;
                }

                case 2:
                    Bang_Thue(carOne,carTwo,carThree);
                    break;


            }
                }


    }
}
