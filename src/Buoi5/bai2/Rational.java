package Buoi5.bai2;
//Bài 2: Một phân số gồm có 2 thuộc tính là tử số (numerator) và mẫu số (denominator). Hãy thiết kế class Phân số (Rational) để thực hiện các chức năng sau:
//        - Tối giản phân số (reduce)
//        - Nghịch đảo phân số (reciprocal)
//        - Cộng 2 phân số (add)
//        - Trừ 2 phân số (subtract)
//        - Nhân 2 phân số (multiply)
//        - Chia 2 phân số (divide)
//        - So sánh 2 phân số, sử dụng sai số 0.0001(tolerance) để so sánh.

public class Rational {
    private int Tuso;
    private int Mauso;
    private int Tuso2;
    private int Mauso2;


    public  static int UCLN(int a, int b)
    {
        return (b==0) ? a : UCLN(b,a%b);
    }

    public void ToiGianPhanSo()
    {
        int uc = UCLN(this.Tuso,this.Mauso);
        System.out.println(this.Tuso/uc + "/" + this.Mauso/uc);

    }

    public void Reciprocal()
    {
        System.out.println(this.Mauso + "/" + this.Tuso);
    }
    public void Add()
    {
        int tu= this.Tuso*this.Mauso2 + this.Tuso2*this.Mauso;
        int mau = this.Mauso*this.Mauso2;

        System.out.println( this.Tuso + "/"+ this.Mauso + " + " + this.Tuso2 + "/" + this.Mauso2 + "  = "+ tu + "/" + mau);
    }
    public void Subtract()
    {
        int tu= this.Tuso*this.Mauso2 - this.Tuso2*this.Mauso;
        int mau = this.Mauso*this.Mauso2;
        System.out.println( this.Tuso + "/"+ this.Mauso + " - " + this.Tuso2 + "/" + this.Mauso2 + "  = "+ tu + "/" + mau);
    }
    public void Multiply()
    {
        int tu= this.Tuso*this.Tuso2;
        int mau = this.Mauso*this.Mauso2;
        System.out.println( this.Tuso + "/"+ this.Mauso + " X " + this.Tuso2 + "/" + this.Mauso2 + "  = "+ tu + "/" + mau);
    }
    public void Divine()
    {
        int tu= this.Tuso*this.Mauso2;
        int mau = this.Mauso*this.Tuso2;
        System.out.println( this.Tuso + "/"+ this.Mauso + " : " + this.Tuso2 + "/" + this.Mauso2 + "  = "+ tu + "/" + mau);
    }
    public void Comparisons()
    {
        float sothunhat = (float) this.Tuso/this.Mauso;
        float sothuhai = (float) this.Tuso2/this.Mauso2;
        float hieu = sothunhat-sothuhai;

        System.out.println("Phan so thu nhat ");
        if(hieu > 0 )
        {
            System.out.println("Lon");
        }

    }


// construction
    public Rational(int tuso, int mauso) {
        Tuso = tuso;
        Mauso = mauso;
    }

    // get and set
    public int getTuso() {
        return Tuso;
    }

    public void setTuso(int tuso) {
        Tuso = tuso;
    }

    public int getMauso() {
        return Mauso;
    }

    public void setMauso(int mauso) {
        Mauso = mauso;
    }



}
