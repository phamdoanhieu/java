package Buoi5.bai7;
//Bài 7: Xây dựng chương trình quản lý danh sách hoá đơn tiền điện của khách hàng. Thông tin bao gồm các loại khách hàng :
//
//        2. Khách hàng nước ngoài:, quốc tịch, số lượng, đơn giá. Thành tiền được tính = số lượng * đơn giá.
//        Thực hiện các yêu cầu sau:
//        Xây dựng các lớp với chức năng thừa kế.
//        1. Nhập xuất danh sách các hóa đơn khách hàng.
//        2. Tính tổng số lượng cho từng loại khách hàng.
//        3. Tính trung bình thành tiền của khách hàng người nước ngoài.
//        4. Xuất ra các hoá đơn trong tháng 09 năm 2013 (cùa cả 2 loại khách hàng).

import java.util.Scanner;

public class Main {
    public static void Menu()
    {
        System.out.println("\n\t\t--------------------------------------------------------");
        System.out.println("\t\t|0. Thoat ung dung                                        |");
        System.out.println("\t\t|1. Thêm khách hàng việt nam                              |");
        System.out.println("\t\t|2. Them khách hàng nước ngoài                            |");
        System.out.println("\t\t|3. Tổng số lượng từng loại khách hàng.                   |");
        System.out.println("\t\t|4. Trung bình thành tiền của khách hàng người nước ngoài |");
        System.out.println("\t\t|5. Xuất Danh Sách                                        |");
        System.out.println("\t\t|6. Xuất ra các hoá đơn trong tháng 09 năm 2013           |");
        System.out.println("\t\t ---------------------------------------------------------");
        System.out.println("Mời chọn: ");
    }
    public static void main(String[] args) {
        QuanLyKH quanLyKH = new QuanLyKH();
        Scanner sc = new Scanner(System.in);
        int chon;
        while (true) {
            Menu();
            chon = sc.nextInt();
            switch (chon) {
                case 1:
                    quanLyKH.NhapKH(1);
                    break;
                case 2:
                    quanLyKH.NhapKH(2);
                    break;
                case 3:
                    quanLyKH.Dem_KH();
                    break;
                case 4:
                    quanLyKH.TrungBinhThanhTien_NN();
                    break;
                case 5:
                    quanLyKH.XuatDs();
                    break;
                case 6:
                    quanLyKH.XuatDs("9/2013");
                    break;
            }
            if (chon == 0)
                return;
        }
    }
}
