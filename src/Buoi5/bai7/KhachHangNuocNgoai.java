package Buoi5.bai7;
//2. Khách hàng nước ngoài: mã khách hàng, họ tên, ngày ra hoá đơn (ngày, tháng, năm), quốc tịch, số lượng, đơn giá.
// Thành tiền được tính = số lượng * đơn giá.
public class KhachHangNuocNgoai extends KhachHang {
   private String QuocTich;

    public KhachHangNuocNgoai(String maKhachHang, String hoTen, String ngayHoaDon, float soLuong, float donGia, String quocTich) {
        super(maKhachHang, hoTen, ngayHoaDon, soLuong, donGia);
        QuocTich = quocTich;
    }

    public KhachHangNuocNgoai() {
        super();
        QuocTich = "";
    }

    public String getQuocTich() {
        return QuocTich;
    }

    public void setQuocTich(String quocTich) {
        QuocTich = quocTich;
    }

    @Override
    public void Nhap()
    {
        super.Nhap();
        System.out.println("Nhập quốc tịch: ");
        QuocTich = sc.nextLine();
    }

    public double ThanhTien()
    {
        //- Nếu số lượng <= định mức thì: thành tiền = số lượng * đơn giá.
        return SoLuong*DonGia;
    }
    @Override
    public String toString() {
        return "[Khách hàng nước ngoài: " + super.toString() + ", Quốc tịch: " + QuocTich + ", Thành tiền: " + ThanhTien() + "]";
    }
}
