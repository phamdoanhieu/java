package Buoi5.bai7;

import java.util.Scanner;

//mã khách hàng, họ tên, ngày ra hoá đơn (ngày, tháng, năm),

public class KhachHang {
    protected String MaKhachHang;
    protected String HoTen;
    protected String NgayHoaDon;
    protected float SoLuong;
    protected float DonGia;


    //

    Scanner sc = new Scanner(System.in);
    public KhachHang(String maKhachHang, String hoTen, String ngayHoaDon, float soLuong, float donGia) {
        MaKhachHang = maKhachHang;
        HoTen = hoTen;
        NgayHoaDon = ngayHoaDon;
        SoLuong = soLuong;
        DonGia = donGia;

    }

    public KhachHang()
    {
        MaKhachHang = "";
        HoTen = "";
        NgayHoaDon = "";
        SoLuong = 0;
        DonGia = 0;
    }

    public float getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(float soLuong) {
        SoLuong = soLuong;
    }

    public float getDonGia() {
        return DonGia;
    }

    public void setDonGia(float donGia) {
        DonGia = donGia;
    }

    public String getMaKhachHang() {
        return MaKhachHang;
    }

    public void setMaKhachHang(String maKhachHang) {
        MaKhachHang = maKhachHang;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String hoTen) {
        HoTen = hoTen;
    }

    public String getNgayHoaDon() {
        return NgayHoaDon;
    }

    public void setNgayHoaDon(String ngayHoaDon) {
        NgayHoaDon = ngayHoaDon;
    }

    //nhập
    public void Nhap()
    {
        System.out.println("Nhập mã khách hàng: ");
        MaKhachHang = sc.nextLine();
        System.out.println("Nhập Họ tên: ");
        HoTen = sc.nextLine();
        System.out.println("Nhập ngày tháng (dd/mm/yyyy): ");
        NgayHoaDon = sc.nextLine();
        System.out.println("Nhập Số KW tiêu thụ: ");
        SoLuong=sc.nextFloat();
        System.out.println("Nhập đơn giá: ");
        DonGia=sc.nextFloat();
        sc.nextLine();
    }

    @Override
    public String toString() {
        return "Mã khách hàng: " + MaKhachHang + ", Họ tên: " + HoTen + ", Ngày xuất hóa đơn: " + NgayHoaDon + ", Số KW tiêu thụ: " + SoLuong
                + ", Đơn giá: " + DonGia ;
    }

    public double ThanhTien() {
        return 0;
    }
}
