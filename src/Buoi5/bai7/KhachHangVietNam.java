package Buoi5.bai7;
//1. Khách hàng Việt Nam:, đối tượng khách hàng (sinh hoạt, kinh doanh, sản xuất): số lượng (số KW tiêu thụ), đơn giá, định mức.
// Thành tiền được tính như sau:
//        - Nếu số lượng <= định mức thì: thành tiền = số lượng * đơn giá.
//        - Ngược lại thì: thành tiền = số lượng * đơn giá * định mức + số lượng KW vượt định mức * Đơn giá * 2.5.
public class KhachHangVietNam extends KhachHang{
    private String DoiTuong;
    private float DinhMuc;

    public KhachHangVietNam(String maKhachHang, String hoTen, String ngayHoaDon, float soLuong, float donGia, String doiTuong, float dinhMuc) {
        super(maKhachHang, hoTen, ngayHoaDon, soLuong, donGia);
        DoiTuong = doiTuong;
        DinhMuc = dinhMuc;
    }

    public KhachHangVietNam() {
        super();
        DoiTuong = "";
        DinhMuc = 0;
    }

    public String getDoiTuong() {
        return DoiTuong;
    }

    public void setDoiTuong(String doiTuong) {
        DoiTuong = doiTuong;
    }

    public float getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(float soLuong) {
        SoLuong = soLuong;
    }

    public float getDonGia() {
        return DonGia;
    }

    public void setDonGia(float donGia) {
        DonGia = donGia;
    }

    public float getDinhMuc() {
        return DinhMuc;
    }

    public void setDinhMuc(float dinhMuc) {
        DinhMuc = dinhMuc;
    }

    // nhập
    public void Nhap()
    {
        super.Nhap();
        System.out.println("Nhập đối tượng khách hàng (sinh hoạt, kinh doanh, sản xuất): ");
        DoiTuong = sc.nextLine();
        System.out.println("Nhập định mức giới hạn: ");
        DinhMuc=sc.nextFloat();
        sc.nextLine();
    }
    public double ThanhTien()
    {
        //- Nếu số lượng <= định mức thì: thành tiền = số lượng * đơn giá.
        if (SoLuong < DinhMuc)
            return SoLuong*DonGia;
        //- Ngược lại thì: thành tiền = số lượng * đơn giá * định mức + số lượng KW vượt định mức * Đơn giá * 2.5.
        return SoLuong*DonGia*DinhMuc + (SoLuong-DinhMuc)*DonGia*2.5;
    }
    @Override
    public String toString() {
        return "[Khách hàng Việt Nam: " + super.toString() + ", Đối tượng khách hàng: " + DoiTuong + ", Định mức: " + DinhMuc + ",Thành Tiền: "+ ThanhTien() + "]";
    }
}