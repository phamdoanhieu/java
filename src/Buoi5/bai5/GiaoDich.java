package Buoi5.bai5;

import java.util.Scanner;

//Bài 5: Xây dựng chương trình quản lý danh sách các giao dịch. Hệ thống quản lý 2 loại giao dịch:
//        - Giao dịch vàng: Mã giao dịch, ngày giao dịch (ngày, tháng, năm), đơn giá, số lượng, loại vàng. Thành tiền được tính như sau:
//        thành tiền = số lượng * đơn giá.
//        - Giao dịch tiền tệ: Mã giao dịch, ngày giao dịch (ngày, tháng, năm), Đơn giá, số lượng, tỉ giá, loại tiền tệ có 3 loại: tiền Việt Nam, tiền USD, tiền Euro. Thành tiền được tính như sau:
//        - Nếu là tiền USD hoặc Euro thì: thành tiền = số lượng * đơn giá* tỉ giá
//        - Nếu là tiền VN thì: thành tiền = số lượng * đơn giá
//        Thực hiện các yêu cầu sau:
//        1. Xây dựng các lớp với chức năng thừa kế.
//        2. Nhập xuất danh sách các giao dịch.
//        3. Tính tổng số lượng cho từng loại.
//        4. Tính trung bình thành tiền của giao dịch tiền tệ.
//        5. Xuất ra các giao dịch có đơn giá > 1 tỷ.
public class GiaoDich {
    Scanner sc = new Scanner(System.in);
    //Mã giao dịch, ngày giao dịch (ngày, tháng, năm), đơn giá, số lượng
    protected int MaGD,SoLuong;
    protected String NgayGD;
    protected double DonGia,ThanhTien;

    public GiaoDich(int maGD, int soLuong, String ngayGD, double dongia, double thanhTien) {
        MaGD = maGD;
        SoLuong = soLuong;
        NgayGD = ngayGD;
        DonGia = dongia;
        ThanhTien = thanhTien;
    }

    public GiaoDich()
    {
        this.MaGD=0;
        this.NgayGD="";
        this.DonGia=0;
        this.SoLuong=0;
        this.ThanhTien=0;
    }
    protected void nhap()
    {

        System.out.println("Ma Giao Dich: ");
        MaGD = sc.nextInt();
        System.out.println("Ngay Giao Dich: ");
        NgayGD = sc.next();
        System.out.println("Don Gia: ");
        DonGia = sc.nextDouble();
        System.out.println("So Luong: ");
        SoLuong = sc.nextInt();
    }

    @Override
    public String toString()
    {
        return " [MaGD=" + MaGD + ", SoLuong=" + SoLuong + ", NgayGD="
                + NgayGD + ", DonGia=" + DonGia;
    }



    public int getMaGD() {
        return MaGD;
    }

    public void setMaGD(int maGD) {
        MaGD = maGD;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int soLuong) {
        SoLuong = soLuong;
    }

    public String getNgayGD() {
        return NgayGD;
    }

    public void setNgayGD(String ngayGD) {
        NgayGD = ngayGD;
    }

    public double getDongia() {
        return DonGia;
    }

    public void setDongia(double dongia) {
        DonGia = dongia;
    }

    public double getThanhTien() {
        return ThanhTien;
    }

    public void setThanhTien(double thanhTien) {
        ThanhTien = thanhTien;
    }

    public double ThanhTien() {
        return 0;
    }
}
