package Buoi5.bai5;

public class GiaoDichNgoaiTe extends GiaoDich {
    private float TiGia;
    private int LoaiTienTe;


    // constructor
    public GiaoDichNgoaiTe() {
        super();
        this.TiGia=0;
        this.LoaiTienTe=0;
    }

    public GiaoDichNgoaiTe(int maGD, int soLuong, String ngayGD, double dongia, double thanhTien, float tiGia, int loaiTienTe) {
        super(maGD, soLuong, ngayGD, dongia, thanhTien);
        TiGia = tiGia;
        LoaiTienTe = loaiTienTe;
    }

    //get and set

    public float getTiGia() {
        return TiGia;
    }

    public void setTiGia(float tiGia) {
        TiGia = tiGia;
    }

    public int getLoaiTienTe() {
        return LoaiTienTe;
    }

    public void setLoaiTienTe(int loaiTienTe) {
        LoaiTienTe = loaiTienTe;
    }

    // in and out


    @Override
    public void nhap()
    {
        super.nhap();
        System.out.println("Ti Gia: ");
        TiGia=sc.nextFloat();
        System.out.println("Loai Tien Te: ");
        LoaiTienTe=sc.nextInt();

    }
    @Override
    public double ThanhTien()
    {
        if(LoaiTienTe==1)
            return this.ThanhTien=this.DonGia*this.SoLuong;
        else
            return this.ThanhTien=this.DonGia*this.SoLuong*this.TiGia;
    }

    public String toString()
    {
        String temp;
        if(LoaiTienTe==1)
            temp="VND";
        else if(LoaiTienTe==2)
            temp="USD";
        else
            temp="EURO";
        return "Giao Dich Tien Te: " + super.toString() + " Ti Gia: " + TiGia
                + ", Loai Tien Te: " + temp + ", ThanhTien: " + ThanhTien + "]";
    }


}
