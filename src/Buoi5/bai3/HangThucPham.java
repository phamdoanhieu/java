package Buoi5.bai3;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//Bài 3: Viết lớp HangThucPham mô tả một hàng hóa là hàng thực phẩm trong kho của một siêu thị, có các thuộc tính:
// mã hàng (không cho phép sửa, không được để rỗng), tên hàng (không được để rỗng), đơn giá (>0),
// ngày sản xuất và ngày hết hạn (ngày không được để rỗng, ngày hết hạn phải sau ngày sản xuất).
//Ràng buộc chặt chẽ các ràng buộc trên các trường dữ liệu.
// Nếu dữ liệu không hợp lệ thì gán giá trị mặc định cho phép tương ứng của trường đó.
//- Tạo 1 constructor có đầy đủ tham số, 1 constructor có tham số là mã hàng.
//- Viết các phương thức setters/getters.
//- Viết phương thức kiểm tra một hàng thực phẩm đã hết hạn chưa?
//- Phương thức toString, trả về chuỗi chứa thông tin của hàng thực phẩm.
// Trong đó: Định dạng đơn giá có phân cách hàng nghìn. Định dạng kiểu ngày là dd/MM/yyyy.
//Viết lớp cho phần kiểm nghiệm.
public class HangThucPham {
    private String Mahang;
    private String Tenhang;
    private int Dongia;
    private String Ngaysx;
    private String Ngayhethan;


    public String toString() {//Ghi đè phương thức toString()
        NumberFormat nf = new DecimalFormat("#.###"); // format number

        try {
            Date ngaysx=new SimpleDateFormat("dd/MM/yyyy").parse(this.Ngaysx); // chuyển ngày sx về date
            Date ngayhethan=new SimpleDateFormat("dd/MM/yyyy").parse(this.Ngayhethan); // chuyển ngày hết hạn về date
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); // Chuyển về Simple date

            return "[Hàng thực phẩm: Mã hàng: " + this.Mahang + ", Tên mặt hàng: " + this.Tenhang + ", Đơn giá:  " + nf.format(this.Dongia) +
                    ", Ngày sản xuất: " + sdf.format(ngaysx) + ", Ngày hết hạn: " +sdf.format(ngayhethan) + "]";
        } catch (ParseException e) {
            return null;
        }

    }

    public boolean DaHetHan() throws ParseException {
        Calendar cal = Calendar.getInstance(); // khai báo lịch dương
        Date now= (Date) cal.getTime(); // get ngày hiện tại

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date ngayhethan = df.parse(this.Ngayhethan); // chuyển ngày từ string về date

        long tru = ngayhethan.getTime() - now.getTime();
        if(tru <0)
            return true;
        return false;
    }

    public String getMahang() {
        return Mahang;
    }

    public void setMahang(String mahang) {
        Mahang = mahang;
    }

    public String getTenhang() {
        return Tenhang;
    }

    public void setTenhang(String tenhang) {
        Tenhang = tenhang;
    }

    public int getDongia() {
        return Dongia;
    }

    public void setDongia(int dongia) {
        Dongia = dongia;
    }

    public String getNgaysx() {
        return Ngaysx;
    }

    public void setNgaysx(String ngaysx) {
        Ngaysx = ngaysx;
    }

    public String getNgayhethan() {
        return Ngayhethan;
    }

    public void setNgayhethan(String ngayhethan) {
        Ngayhethan = ngayhethan;
    }
}
