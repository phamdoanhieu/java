package Buoi5.bai3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Main {
    public static void main(String[] args) throws ParseException {

//
//        HangThucPham hangThucPham;
//        hangThucPham = new HangThucPham("1","test",10000000,"21/9/2019","22/9/2019");
//
//        // kiểm tra hết hạn
//        System.out.println("Da het han ? " + hangThucPham.DaHetHan());
//
//        // kiểm tra phương thức toString;
//        System.out.println(hangThucPham.toString());

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date ngayhethan = df.parse("21/04/2000"); // chuyển ngày từ string về date

        System.out.println(ngayhethan); // biến thành số

    }

}
